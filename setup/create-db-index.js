'use strict';

const _init = require('../src/cmd/_init');
const Book = require('../src/database/mongo/models/Book');
const Website = require('../src/database/mongo/models/Website');
const InsertionBatch = require('../src/database/mongo/models/InsertionBatch');

_init()
  .then(run)
  .then(process.exit);


async function run () {
  console.log('\n-> Setting indexes for [books] table');
  await setupIndexesForBooksTable();

  console.log('\n-> Setting indexes for [websites] table');
  await setupIndexesForWebsitesTable();

  console.log('\n-> Setting indexes for [insertion_batches] table');
  await setupIndexesForInsertionBatchesTable();
}


async function setupIndexesForBooksTable () {
  const promises = [];

  // Index [libgen_details_url]
  promises.push(
    Book.collection.createIndex('libgen_details_url', {
      name  : 'libgen_details_url_unq',
      unique: true,
      partialFilterExpression: {
        libgen_details_url: {
          $exists: true,
          $type  : 'string',
        }
      },
    }).then(value => console.log('Created index [libgen_details_url_unq].', value))
      .catch(err => console.error('Failed to create index [libgen_details_url_unq].', err))
  );

  // Index [isbn10_unq];
  promises.push(
    Book.collection.createIndex('isbn10', {
      name  : 'isbn10_unq',
      unique: true,
      partialFilterExpression: {
        isbn10: {
          $exists: true,
          $type  : 'string',
        }
      },
    }).then(value => console.log('Created index [isbn10_unq].', value))
      .catch(err => console.error('Failed to create index [isbn10_unq].', err))
  );

  // Index [isbn13_unq];
  promises.push(
    Book.collection.createIndex('isbn13', {
      name  : 'isbn13_unq',
      unique: true,
      partialFilterExpression: {
        isbn13: {
          $exists: true,
          $type  : 'string',
        }
      },
    }).then(value => console.log('Created index [isbn13_unq].', value))
      .catch(err => console.error('Failed to create index [isbn13_unq].', err))
  );

  // Index [isbn10_related];
  promises.push(
    Book.collection.createIndex('isbn10_related', { name: 'isbn10_related' })
      .then(value => console.log('Created index [isbn10_related].', value))
      .catch(err => console.error('Failed to create index [isbn10_related].', err))
  );

  // Index [isbn13_related];
  promises.push(
    Book.collection.createIndex('isbn13_related', { name: 'isbn13_related' })
      .then(value => console.log('Created index [isbn13_related].', value))
      .catch(err => console.error('Failed to create index [isbn13_related].', err))
  );

  // Index [fts] full-text-search;
  promises.push(
    Book.collection.createIndex({ title: 'text' }, { name: 'fts' })
      .then(value => console.log('Created index [fts].', value))
      .catch(err => console.error('Failed to create index [fts].', err))
  );

  // Index [insertion_batch_id];
  promises.push(
    Book.collection.createIndex('insertion_batch_id', { name: 'insertion_batch_id' })
      .then(value => console.log('Created index [insertion_batch_id].', value))
      .catch(err => console.error('Failed to create index [insertion_batch_id].', err))
  );

  await Promise.all(promises);
}

async function setupIndexesForWebsitesTable ()  {
  const promises = [];

  promises.push(
    Website.collection.createIndex('name', {
      name: 'name_unq',
      unique: true,
      partialFilterExpression: {
        name: {
          $exists: true,
          $type  : 'string',
        }
      },
    })
      .then(value => console.log('Created index [name_unq].', value))
      .catch(err  => console.error('Failed to create index [name_unq].', err))
  );

  promises.push(
    Website.collection.createIndex('url', {
      name: 'url_unq',
      unique: true,
      partialFilterExpression: {
        url: {
          $exists: true,
          $type  : 'string',
        }
      },
    })
      .then(value => console.log('Created index [url_unq].', value))
      .catch(err  => console.error('Failed to create index [url_unq].', err))
  );

  await Promise.all(promises);
}

async function setupIndexesForInsertionBatchesTable () {
  const promises = [];

  promises.push(
    InsertionBatch.collection.createIndex('name', {
      name: 'name_unq',
      unique: true,
      partialFilterExpression: {
        name: {
          $exists: true,
          $type:   'string',
        }
      },
    })
      .then(value => console.log('Created index [name_unq].', value))
      .catch(err  => console.error('Failed to create index [name_unq].', err))
  );

  await Promise.all(promises);
}
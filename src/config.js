'use strict';

const FastestValidator = require('fastest-validator');

const config = {
  serverApi: {
    host: process.env.SERVER_API_HOST || 'localhost',
    port: process.env.SERVER_API_PORT || 3001,
  },
  serverBull: {
    host: process.env.SERVER_BULL_HOST || 'localhost',
    port: process.env.SERVER_BULL_PORT || 3002,
  },
  mongo: {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    db  : process.env.MONGO_DB   || 'pinz_boutik',
    user: process.env.MONGO_USER,
    pwd : process.env.MONGO_PWD,
  },
  b2: {
    appKey  : process.env.B2_APP_KEY,
    appKeyId: process.env.B2_APP_KEY_ID,
    bucketIdBook : process.env.B2_BUCKET_ID_BOOK,
    bucketIdImage: process.env.B2_BUCKET_ID_IMAGE,
  },
  proxy: {
    host:     process.env.PROXY_HOST,
    port:     process.env.PROXY_PORT,
    disabled: process.env.PROXY_DISABLED || false,
  },
};

config.validate = () => {
  const validator = new FastestValidator;
  const validationResult = validator.validate(config, {
    b2: {
      type: 'object',
      props: {
        appKey       : 'string',
        appKeyId     : 'string',
        bucketIdBook : 'string',
        bucketIdImage: 'string',
      },
    },
    proxy: {
      type: 'object',
      props: {
        host    : 'string',
        port    : { type: 'number', convert : true },
        disabled: { type: 'boolean', convert: true },
      },
    },
  });

  if (validationResult instanceof Object) {
    const err = new Error('Config is invalid');
    err.validationErrors = validationResult;
    throw err;
  }
};

module.exports = config;

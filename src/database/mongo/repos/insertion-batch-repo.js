'use strict';

const Book = require('../models/Book');
const InsertionBatch = require('../models/InsertionBatch');

module.exports = {
  insert,
  exist,
  deleteById,
  getList,
}

/**
 * @param {String} name
 * @returns {Promise<InsertionBatch>}
 */
async function insert (name) {
  return await InsertionBatch.create({ name });
}

/**
 * @param {String} name
 */
async function exist (name) {
  return await InsertionBatch.exists({ name });
}

/**
 * @param {String} id
 */
async function deleteById (id) {
  await InsertionBatch.deleteOne({ _id: id }).exec();
}

async function getList () {
  const data = await InsertionBatch.find().exec();
  const total = await InsertionBatch.count().exec();
  return { total, data };
}
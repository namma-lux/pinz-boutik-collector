'use strict';

const bcrypt = require('bcrypt');
const User = require('../models/User');

module.exports = {
  findByEmail,
  findBySimpleAuth,
};

/**
 * @param {String} email
 */
async function findByEmail (email) {
  return await User.findOne({ email }).exec();
}

/**
 * Find user by email, then compare password by bcrypt.
 *
 * @param {String} email
 * @param {String} password
 */
async function findBySimpleAuth (email, password) {
  const user = await findByEmail(email);
  if (! user)
    return null;

  const hashed = user.password;
  const matched = await bcrypt.compare(password, hashed);
  if (matched)
    return user;

  return null;
};
'use strict';

const Website = require('../models/Website');

module.exports = { findById, getList, exists }


async function findById (id, throwIfNotFound = false) {
  const website = await Website.findById(id).exec();
  if (! website && throwIfNotFound)
    throw new Error(`Website ID [${id}] not found.`);

  return website;
}

async function getList () {
  const data  = await Website.find().exec();
  const total = data.length;

  return { total, data };
}

async function exists (_id) {
  return await Website.exists({ _id });
}
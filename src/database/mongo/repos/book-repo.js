'use strict';

const flatten = require('flat');
const BookType = require('../../../const/BookType');
const BookRating = require('../../../const/BookRating');
const Book = require('../models/Book');

const DATA_COMPLETION_STATUS = {
  INCOMPLETE              : 'incomplete',
  IMAGE_NOT_COLLECTED     : 'image_not_collected',
  FILE_NOT_COLLECTED      : 'file_not_collected',
  IMAGE_COLLECTED         : 'image_collected',
  IMAGE_COLLECTED_NOT_FILE: 'image_collected_not_file',
  FILE_COLLECTED          : 'file_collected',
  FILE_COLLECTED_NOT_IMAGE: 'file_collected_not_image',
  COMPLETED               : 'completed',

  values: () => Object.values(DATA_COMPLETION_STATUS),
};

module.exports = {
  DATA_COMPLETION_STATUS,

  insert,
  update,
  updateB2InfoForBookFile,
  updateB2InfoForBookImageFile,
  updateBookRating,
  updateByLibgenExtractedData,
  addMetadataCategory,
  deleteById,
  getBookTypeById,
  getIsbnCoupleById,
  getList,
  countForBatches,
  findById,
  findByIsbn,
  findByIsbn10,
  findByIsbn13,
  findByLibgenDetailsUrl,
  exist,
  existStandardBook,
};

/**
 * @param {Object} data Simple JSON object containing Book data
 * @param {String} employeeId
 * @returns {Promise<Book>}
 */
async function insert (data, employeeId = null) {
  if (data.file && data.file.md5)
    data.libgen_details_url = buildLibgenDetailsURLFromMD5(data.file.md5);

  const now = new Date;

  const newData = {
    insertion_batch_id: data.insertion_batch_id || undefined,

    title         : data.title || null,
    type          : data.type || null,
    isbn10        : data.isbn10 || null,
    isbn13        : data.isbn13 || null,
    isbn10_related: data.isbn10_related || null,
    isbn13_related: data.isbn13_related || null,

    libgen_details_url: data.libgen_details_url || undefined,

    history: {
      inserted_at      : now,
      modified_at      : now,
      inserted_by      : employeeId,
      last_fetch_libgen: null,
      last_image_upload: null,
      last_file_upload : null,
    },

    metadata: {
      category      : null,
      author        : null,
      description   : null,
      edition       : null,
      series        : null,
      publisher     : null,
      published_year: null,
      language      : null,
      pages_count   : null,
      price         : null,
      rating        : null,
      ...(data.metadata || {})
    },

    image: {
      ext       : null,
      source_url: null,
      b2_file_id: null,
      b2_url    : null,
      ...(data.image || {})
    },

    file: {
      size      : null,
      ext       : null,
      md5       : null,
      source_url: null,
      b2_file_id: null,
      b2_url    : null,
      ...(data.file || {})
    },
  };

  return await Book.create(newData);
}

/**
 * @param {String} id
 * @param {Object} data
 */
async function update (id, data) {
  if (data.file && data.file.md5)
    data.libgen_details_url = buildLibgenDetailsURLFromMD5(data.file.md5);

  const updatedBook = await Book.findByIdAndUpdate(id,
    { $set: flatten(data) },
    { new: true }
  ).exec();

  return updatedBook;
}

/**
 * @param {string} bookId
 * @param {string} b2FileId
 * @param {string} b2DownloadUrl
 * @param {?string} fileExt
 */
async function updateB2InfoForBookFile (bookId, b2FileId, b2DownloadUrl, fileExt = undefined) {
  const file = {
    b2_file_id: b2FileId,
    b2_url:     b2DownloadUrl,
  };
  if (fileExt)
    file.ext = fileExt;

  const history = { last_file_upload: new Date };

  return await update(bookId, { history, file });
}

/**
 * @param {string} bookId
 * @param {string} b2FileId
 * @param {string} b2DownloadUrl
 * @param {?string} fileExt
 */
async function updateB2InfoForBookImageFile (bookId, b2FileId, b2DownloadUrl, fileExt = undefined) {
  const image = {
    b2_file_id: b2FileId,
    b2_url:     b2DownloadUrl,
  };
  if (fileExt)
    image.ext = fileExt;

  const history = { last_image_upload: new Date };

  return await update(bookId, { history, image });
}

/**
 * @param {String} id
 * @param {Object} rating
 */
async function updateBookRating (id, rating) {
  if (! BookRating.isValid(rating))
    throw new Error(`Book-rating value [${rating}] is not supported.`);

  const metadata = { rating };
  return await update(id, { metadata });
}

/**
 * @param {String} id
 * @param {Object} extractedData
 */
async function updateByLibgenExtractedData (id, extractedData) {
  const history = { last_fetch_libgen: new Date };
  return await update(id, { ...extractedData, history });
}

async function addMetadataCategory (id, newCategory) {
  const SEPARATOR = ',';

  const book       = await findById(id, true);
  const categories = book.metadata.category ? book.metadata.category.split(SEPARATOR) : [];

  if (categories.includes(newCategory))
    return null;

  categories.push(newCategory);
  const metadata = { category: categories.join(SEPARATOR) }
  return await update(id, { metadata });
}

async function deleteById (id) {
  await Book.deleteOne({ _id: id }).exec();
}

async function getBookTypeById (id) {
  const book = await Book.findById(id, { type: 1 })
  if (! book)
    throw new Error(`Book ID [${id}] not found.`);

  return book.type;
}

async function getIsbnCoupleById (id) {
  const projection = {
    type: 1,
    isbn10: 1,
    isbn13: 1,
    isbn10_related: 1,
    isbn13_related: 1,
  };

  const book = await Book.findById(id, projection).exec();
  if (! book)
    throw new Error(`Book ID [${id}] not found.`);

  let isbn10, isbn13;
  if (BookType.BOOK == book.type) {
    isbn10 = book.isbn10;
    isbn13 = book.isbn13;
  } else {
    isbn10 = book.isbn10_related;
    isbn13 = book.isbn13_related;
  }
  return { isbn10, isbn13 };
}

async function countForBatches (insertionBatchIds, isCompleted = false) {
  const filters = {
    // At least must belongs to an insertion-batch
    insertion_batch_id: { $in: insertionBatchIds },
  };

  if (isCompleted) {
    Object.assign(filters, {
      // Completed state filter
      title         : { $ne: null },
      'image.b2_url': { $ne: null },
      'file.b2_url' : { $ne: null },
    })
  }

  return await Book.aggregate([
    {
      $match: filters
    },
    {
      $group: {
        _id  : '$insertion_batch_id',
        count: { $sum: 1 },
      }
    }
  ]).exec();
}

async function findById (id, throwIfNotFound = false) {
  const book = await Book.findById(id).exec();
  if (! book && throwIfNotFound)
    throw new Error(`Book ID [${id}] not found.`);

  return book;
}

async function getList (options) {
  const { paging, filter, sorting, lookups } = options;

  let queryConditions = undefined;
  if (filter) {
    const $and = [];

    if (filter._id) {
      if (filter._id instanceof Array) {
        $and.push({ _id: { $in: filter._id } });
      } else {
        $and.push({ _id: filter._id });
      }
    }

    if (filter.isbn) {
      const $regex = new RegExp(filter.isbn);
      $and.push({
        $or: [
          { isbn10        : { $regex } },
          { isbn13        : { $regex } },
          { isbn10_related: { $regex } },
          { isbn13_related: { $regex } },
        ]
      });
    }

    if (filter.title) {
      const $regex = new RegExp(filter.title, 'i');
      $and.push({ title: { $regex } });
    }

    if (filter.type) {
      $and.push({ type: filter.type });
    }

    if (filter.insertion_batch_id) {
      $and.push({ insertion_batch_id: filter.insertion_batch_id });
    }

    if (filter.categories) {
      const $in = filter.categories.map(keyword => new RegExp(keyword));
      $and.push({ 'metadata.category': { $in } });
    } else if (filter.category) {
      const $regex = new RegExp(filter.category, 'i');
      $and.push({ 'metadata.category': { $regex } });
    }

    if (filter.inserted_by) {
      $and.push({ 'history.inserted_by': filter.inserted_by });
    }

    if (filter.fetchedLibgen) {
      $and.push({ 'history.last_fetch_libgen': { $ne: null } });
    }

    if (filter.completionStatus) {
      switch (filter.completionStatus) {
        case DATA_COMPLETION_STATUS.INCOMPLETE:
          $and.push({
            $or: [
              { title         : null },
              { 'image.b2_url': null },
              { 'file.b2_url' : null },
            ]
          });
          break;

        case DATA_COMPLETION_STATUS.IMAGE_NOT_COLLECTED:
          $and.push({ 'image.b2_url': null });
          break;

        case DATA_COMPLETION_STATUS.FILE_NOT_COLLECTED:
          $and.push({ 'file.b2_url': null });
          break;

        case DATA_COMPLETION_STATUS.IMAGE_COLLECTED:
          $and.push({
            'image.b2_url': { $ne: null },
          });
          break;

        case DATA_COMPLETION_STATUS.IMAGE_COLLECTED_NOT_FILE:
          $and.push({
            'image.b2_url': { $ne: null },
            'file.b2_url' : null,
          });
          break;

        case DATA_COMPLETION_STATUS.FILE_COLLECTED:
          $and.push({
            'file.b2_url': { $ne: null },
          });
          break;

        case DATA_COMPLETION_STATUS.FILE_COLLECTED_NOT_IMAGE:
          $and.push({
            'file.b2_url' : { $ne: null },
            'image.b2_url': null,
          });
          break;

        case DATA_COMPLETION_STATUS.COMPLETED:
          $and.push({
            title         : { $ne: null },
            'image.b2_url': { $ne: null },
            'file.b2_url' : { $ne: null },
          });
          break;

        default:
          throw new Error(`Completion-status [${filter.completionStatus}] not allowed.`);
      }
    }

    queryConditions = { $and };
  }

  const query = Book.find(queryConditions);
  if (paging) {
    const skipped = paging.size * paging.index;
    query.skip(skipped).limit(paging.size);
  }
  if (sorting) {
    query.sort(sorting);
  }
  if (lookups) {
    for (const item of lookups) {
      const [path, selection] = item.split(':');
      query.populate(path, selection);
    }
  }
  const data = await query.exec();

  const total = await Book.count(queryConditions).exec();

  return { total, data };
}

/**
 * @param {String} isbn
 * @returns {Array<Book>}
 */
async function findByIsbn (isbn) {
  if (isbn.length == 10)
    return findByIsbn10(isbn);

  if (isbn.length == 13)
    return findByIsbn13(isbn);

  throw new Error('Invalid ISBN length');
}

/**
 * @param {String} isbn10
 * @returns {Array<Book>}
 */
async function findByIsbn10 (isbn10) {
  if (isbn10.length != 10)
    throw new Error('Invalid ISBN length');

  const book = await Book.find({
    $or: [
      { isbn10 },
      { isbn10_related: isbn10 },
    ]
  }).exec();
  return book;
}

/**
 * @param {String} isbn13
 * @returns {<Array<Book>}
 */
async function findByIsbn13 (isbn13) {
  if (isbn13.length != 13)
    throw new Error('Invalid ISBN length');

  const book = await Book.find({
    $or: [
      { isbn13 },
      { isbn13_related: isbn13 },
    ]
  }).exec();
  return book;
}

/**
 * @param {String} libgen_details_url
 * @return {Promise<?Book>}
 */
async function findByLibgenDetailsUrl (libgen_details_url) {
  const book = await Book.findOne({ libgen_details_url });
  return book;
}

/**
 * @param {String} _id
 */
async function exist (_id) {
  return await Book.exists({ _id });
}

/**
 * @param {Object} isbnMap
 * @param {String} excludedId
 */
async function existStandardBook (isbnMap, excludedId = undefined) {
  const { isbn10, isbn13 } = isbnMap;
  const isbnConditions = [];
  if (isbn10)
    isbnConditions.push({ isbn10 });
  if (isbn13)
    isbnConditions.push({ isbn13 });

  const queryConditions = {
    type: BookType.BOOK,
    $or: isbnConditions,
  };

  if (excludedId)
    queryConditions._id = { $ne: excludedId };

  return await Book.exists(queryConditions);
}


/**
 * @param {String} md5
 */
function buildLibgenDetailsURLFromMD5 (md5) {
  return `http://libgen.rs/book/index.php?md5=${md5}`;
}
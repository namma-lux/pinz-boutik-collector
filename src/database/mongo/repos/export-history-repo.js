'use strict';

const ExportHistory = require('../models/ExportHistory');

module.exports = { findByWebsiteId, insert };


async function findByWebsiteId (website_id) {
  return await ExportHistory.find({ website_id }).exec();
}

async function insert (data) {
  return await ExportHistory.create(data);
}
'use strict';

const mongoose = require('mongoose');

module.exports =
  mongoose.model('InsertionBatches', new mongoose.Schema({
    name: {
      type:     String,
      required: true,
    },
    inserted_at: {
      type:     Date,
      required: true,
      default:  Date.now,
    },
    updated_at: {
      type:     Date,
      required: true,
      default:  Date.now,
    },
  }, { collection: 'insertion_batches' }));
'use strict';

const mongoose = require('mongoose');

module.exports =
  mongoose.model('Website', new mongoose.Schema({
    name: {
      type    : String,
      required: true,
    },
    url: {
      type:     String,
      required: true,
    },
  }));

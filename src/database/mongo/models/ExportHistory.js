'use strict';

const mongoose = require('mongoose');

module.exports =
  mongoose.model('ExportHistory', new mongoose.Schema({
    website_id: {
      type:     mongoose.Schema.Types.ObjectId,
      required: true,
    },
    book_category: {
      type:     String,
      required: true,
    },
    date: {
      type:     Date,
      required: true,
      default:  Date.now,
    },
  }, { collection: 'export_histories' }));

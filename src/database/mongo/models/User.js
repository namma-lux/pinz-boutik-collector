'use strict';

const mongoose = require('mongoose');

module.exports =
  mongoose.model('User', new mongoose.Schema({
    email: {
      type    : String,
      required: true,
    },
    password: {
      type    : String,
      required: true,
    },
    name: {
      type    : String,
      required: true,
    },
    is_master: {
      type: Boolean,
    },
    roles: {
      type   : [String],
      default: [],
    },
  }));

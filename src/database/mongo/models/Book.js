'use strict';

const mongoose = require('mongoose');
const BookType = require('../../../const/BookType');

module.exports =
  mongoose.model('Book', new mongoose.Schema({
    insertion_batch_id: { type: mongoose.Schema.Types.ObjectId, ref: 'InsertionBatch' },

    title: String,
    type: {
      type: String,
      enum: BookType.getAvailableValues(),
    },
    isbn10: String,
    isbn13: String,
    isbn10_related: String,
    isbn13_related: String,
    libgen_details_url: String,

    history: {
      inserted_at      : Date,
      modified_at      : Date,
      inserted_by      : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      last_fetch_libgen: Date,
      last_image_upload: Date,
      last_file_upload : Date,
    },

    metadata: {
      category      : String,
      author        : String,
      description   : String,
      edition       : String,
      series        : String,
      publisher     : String,
      published_year: Number,
      language      : String,
      pages_count   : Number,
      price         : Number,
      rating        : String,
    },

    image: {
      ext       : String,
      source_url: String,
      b2_file_id: String,
      b2_url    : String,
    },

    file: {
      size      : Number,
      ext       : String,
      sha1      : String,
      md5       : String,
      source_url: String,
      b2_file_id: String,
      b2_url    : String
    }
  }));

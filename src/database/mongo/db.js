'use strict';

const mongoose = require('mongoose');

module.exports = { connect };

async function connect (host, port, db, user, pwd) {
  const connString = createConnectionString(host, port, db, user, pwd);
  await mongoose.connect(connString, {
    useNewUrlParser   : true,
    useUnifiedTopology: true,
  });
}

function createConnectionString (host, port, db, user, pwd) {
  let connStr = 'mongodb://';

  if (user) {
    if (pwd)
      connStr += `${user}:${pwd}@`;
    else
      connStr += `${user}@`;
  }

  if (port)
    connStr += `${host}:${port}/${db}`;
  else
    connStr += `${host}/${db}`;

  return connStr;
}

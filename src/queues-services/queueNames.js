'use strict';

exports.DOWNLOAD_BOOK       = 'download-book';
exports.DOWNLOAD_SMALL_FILE = 'download-small-file';
exports.FETCH_LIBGEN        = 'fetch-libgen';

'use strict';

const Bull = require('bull');
const queueNames = require('./queueNames');

/** @type {Map<String, import('bull').Queue>} */
const instanceMap = new Map();

module.exports = {
  getQueueFetchLibgen,
  getQueueDownloadBook,
  getQueueDownloadSmallFile,
};


function getQueueFetchLibgen () {
  return getSingleton(queueNames.FETCH_LIBGEN);
}

function getQueueDownloadBook () {
  return getSingleton(queueNames.DOWNLOAD_BOOK);
}

function getQueueDownloadSmallFile () {
  return getSingleton(queueNames.DOWNLOAD_SMALL_FILE);
}

function getSingleton (name) {
  let instance = instanceMap.get(name);

  if (! instance) {
    // TODO Improve this;
    instanceMap.set(name, instance = new Bull(name, {
      prefix: name,
      limiter: {
        max: 1,
        duration: 2000,
      }
    }));
  }

  return instance;
}
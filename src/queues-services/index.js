'use strict';

exports.queueNames = require('./queueNames');

exports.downloadBook      = require('./download-book-queue/service');
exports.downloadSmallFile = require('./download-small-file-queue/service');
exports.fetchLibgen       = require('./fetch-libgen-queue/service');

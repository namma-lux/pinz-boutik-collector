'use strict';

const queueFactory = require('../bullQueueFactory');
const SmallFileType = require('./SmallFileType');
const worker = require('./worker');

let isWorking = false;
const setWorkingStatus = status => isWorking = status;

module.exports = {
  worker: { start, stop },
  pushJobDownloadBookImage,
};


async function start () {
  if (! isWorking) {
    queueFactory.getQueueDownloadSmallFile().process(1, worker);
    setWorkingStatus(true);
  }
}

async function stop () {
  await queueFactory.getQueueDownloadSmallFile().pause(true, false);
  setWorkingStatus(false);

  await queueFactory.getQueueDownloadSmallFile().close();
}

async function pushJobDownloadBookImage (bookId) {
  await queueFactory.getQueueDownloadSmallFile().add({ fileType: SmallFileType.BOOK_IMAGE, bookId }, { attempts: 3 });
}

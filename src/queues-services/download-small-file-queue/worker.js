'use strict';

const fs = require('fs');
const axios = require('axios').default;
const axiosRetryPlugin = require('axios-retry');
const mime = require('mime-types');
const B2Client = require('../../file-services/backblaze/Client');
const bookRepo = require('../../database/mongo/repos/book-repo');
const config = require('../../config');
const localStorage = require('../../file-services/local-storage');
const SmallFileType = require("./SmallFileType");

const B2_APP_KEY    = config.b2.appKey;
const B2_APP_KEY_ID = config.b2.appKeyId;
const B2_BUCKET_ID  = config.b2.bucketIdImage;

const REQUEST_TIMEOUT     = 1000 * 15;
const REQUEST_RETRY_DELAY = 1000 * 2;
const axiosClient         = createAxiosClient();

const fileTypeToHandlerMap = {
  [SmallFileType.BOOK_IMAGE]: handleDownloadBookImage,
};

/**
 * @param {import("bull").Job} job
 */
module.exports = async function workerDownloadSmallFile (job) {
  const { fileType } = job.data;
  const handler      = fileTypeToHandlerMap[fileType];

  if (! handler)
    throw new Error(`No handler available for small-file-type [${fileType}]`)

  await handler.call(null, job);
}

/**
 * @param {import('bull').Job} job
 */
async function handleDownloadBookImage (job) {
  const { bookId } = job.data;

  const book = await bookRepo.findById(bookId);
  if (! book)
    throw new Error(`Book ID [${bookId}] not found.`);

  const sourceUrl = book.image.source_url;
  if (! sourceUrl) {
    // TEMPORARY;
    // throw new Error(`Book ID [${bookId}] have no image.source_url.`);
    console.error(new Error(`Book ID [${bookId}] have no image.source_url.`));
    return;
  }

  const localFilePath = localStorage.getPathDownloadedFile(bookId);
  const resp          = await requestForDownloadStream(sourceUrl);
  const fileExt       = getFileExtensionFromContentType(resp);

  unlinkSyncIfExists(localFilePath);
  console.log(`Book ID [${bookId}] Downloading book-image file [${localFilePath}].`);
  await download(resp, localFilePath, job);
  console.log(`Book ID [${bookId}] Downloaded book-image file.`);

  try {
    console.log(`Book ID [${bookId}] Uploading book-image file`);
    const destFilename      = `${bookId}.${fileExt}`;
    const uploadRespPayload = await upload(localFilePath, destFilename);
    console.log(`Book ID [${bookId}] Uploaded book-image file`);

    const { fileId, downloadUrl } = uploadRespPayload;
    await bookRepo.updateB2InfoForBookImageFile(bookId, fileId, downloadUrl, fileExt);
    console.log(`Book ID [${bookId}] Updated B2 file-id of book-image file.`);

  } catch (err) {
    await job.discard();
    throw err;

  } finally {
    unlinkSyncIfExists(localFilePath);
  }
}


/**
 * @param {String} sourceUrl
 */
async function requestForDownloadStream (sourceUrl) {
  const MAX_ATTEMPTS = 3;
  let attempts       = 0;
  let resp           = null;
  const raiseAttempts = () => ++attempts;
  const shouldRetry   = () => attempts < MAX_ATTEMPTS;
  const waitFor       = async ms => await new Promise(resolve => setTimeout(resolve, ms));

  do {
    resp = await axiosClient.get(sourceUrl, { responseType: 'stream' });

    const fileExt    = getFileExtensionFromContentType(resp);
    const acceptable = (fileExt != 'html');
    if (acceptable)
      return resp;

    raiseAttempts();
    if (shouldRetry())
      await waitFor(REQUEST_RETRY_DELAY);

  } while (shouldRetry());

  const err = new Error('Could not request for download-stream.');
  err.response = resp;
  throw err;
}

/**
 * Get file-extension from response header [Content-Type];
 *
 * @param {import('axios').AxiosResponse} resp
 */
function getFileExtensionFromContentType (resp) {
  const contentType = resp.headers['content-type'];

  const fileExt = mime.extension(contentType);
  return fileExt;
}

async function download (resp, localFilePath, job) {
  const sourceStream = resp.data;
  const size         = resp.headers['content-length'];

  const updateProgInterval = setUpdateProgressInterval(5000, job, sourceStream, size);
  try {
    await localStorage.saveStreamToFile(sourceStream, localFilePath);
    job.progress(100);

  } finally {
    clearInterval(updateProgInterval);
  }
}

function unlinkSyncIfExists (path) {
  if (fs.existsSync(path))
    fs.unlinkSync(path);
}

function setUpdateProgressInterval (interval, job, downloadStream, byteSize) {
  const prog = {
    total: byteSize,
    recv : 0,
  };

  downloadStream.on('data', chunk => prog.recv += chunk.length);

  const updateProgInterval = setInterval(intervalCallback, interval);
  function intervalCallback () {
    const curPercent = parseFloat((100.0 * prog.recv/prog.total).toFixed(2));
    job.progress(curPercent);
  };

  return updateProgInterval;
}

async function upload (localFilePath, uploadFilename) {
  const b2Client          = B2Client.createInstanceForBucketImages(B2_APP_KEY, B2_APP_KEY_ID, B2_BUCKET_ID);
  const uploadRespPayload = await b2Client.upload(fs.createReadStream(localFilePath), uploadFilename, 5);

  return uploadRespPayload;
}

function createAxiosClient () {
  const client = axios.create({
    timeout: REQUEST_TIMEOUT,
    proxy  : config.proxy.disabled ? undefined : config.proxy,
  });

  axiosRetryPlugin(client, {
    shouldResetTimeout: true,
    retryDelay    : () => REQUEST_RETRY_DELAY,
    retryCondition: () => true, // Retry all-the-types
  });

  return client;
}

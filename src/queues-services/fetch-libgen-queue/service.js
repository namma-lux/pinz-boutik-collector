'use strict';

const queueFactory = require('../bullQueueFactory');
const worker = require('./worker');

let isWorking = false;
const setWorkingStatus = status => isWorking = status;

module.exports = {
  worker: { start, stop },
  pushJobFetchLibgen,
};


async function start () {
  if (! isWorking) {
    queueFactory.getQueueFetchLibgen().process(3, worker);
    setWorkingStatus(true);
  }
}

async function stop () {
  await queueFactory.getQueueFetchLibgen().pause(true, false);
  setWorkingStatus(false);

  await queueFactory.getQueueFetchLibgen().close();
}

async function pushJobFetchLibgen (bookId, noDownload = undefined) {
  await queueFactory.getQueueFetchLibgen().add({ bookId, noDownload }, { attempts: 3 });
}

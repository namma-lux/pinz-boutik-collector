'use strict';

const bookRepo = require('../../database/mongo/repos/book-repo');
const downloadBookQueueService = require('../download-book-queue/service');
const downloadSmallFileQueueService = require('../download-small-file-queue/service');
const libgenImporter = require('../../data-importers/libgen/importer');

/**
 * @param {import("bull").Job} job
 */
module.exports = async function workerFetchLibgen (job) {
  const { bookId, noDownload } = job.data;

  console.log(`Fetching Libgen for Book ID [${bookId}].`);
  try {
    const book = await fetchLibgen(job);
    console.log(`Fetched Libgen for Book ID [${bookId}].`);

    if (noDownload)
      return;

    if (book.image.source_url)
      await downloadSmallFileQueueService.pushJobDownloadBookImage(bookId);

    if (book.file.source_url)
      await downloadBookQueueService.pushJobDownloadBook(bookId);

  } catch (err) {
    if (err instanceof libgenImporter.LibgenNotFoundError) {
      console.error(`Not found on Libgen for Book ID [${bookId}]`);

    } else if (err.code == 11000) {
      // If duplicated (ISBN in-case-of-standard-book | Libgen-details-URL)
      await bookRepo.deleteById(bookId);

    } else {
      throw err;
    }
  }
}

/**
 * @param {import('bull').Job} job
 */
async function fetchLibgen (job) {
  const { bookId } = job.data;

  const book = await bookRepo.findById(bookId);
  if (! book)
    throw new Error(`Book ID [${bookId}] not found.`);

  if (book.libgen_details_url) {
    return await libgenImporter.importByLibgenURL(book);
  } else {
    return await libgenImporter.importByIsbn(book);
  }
}

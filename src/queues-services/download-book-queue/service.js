'use strict';

const queueFactory = require('../bullQueueFactory');
const worker = require('./worker');

let isWorking = false;
const setWorkingStatus = status => isWorking = status;

module.exports = {
  worker: { start, stop },
  pushJobDownloadBook,
};


async function start () {
  if (! isWorking) {
    queueFactory.getQueueDownloadBook().process(3, worker);
    setWorkingStatus(true);
  }
}

async function stop () {
  await queueFactory.getQueueDownloadBook().pause(true, false);
  setWorkingStatus(false);

  await queueFactory.getQueueDownloadBook().close();
}

async function pushJobDownloadBook (bookId) {
  await queueFactory.getQueueDownloadBook().add({ bookId }, { attempts: 3 });
}

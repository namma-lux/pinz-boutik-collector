'use strict';

const fs = require('fs');
const axios = require('axios').default;
const axiosRetryPlugin = require('axios-retry');
const mime = require('mime-types');
const B2Client = require('../../file-services/backblaze/Client');
const bookRepo = require('../../database/mongo/repos/book-repo');
const checksumVerifier = require('../../file-services/checksum-verifier');
const config = require('../../config');
const localStorage = require('../../file-services/local-storage');

const B2_APP_KEY    = config.b2.appKey;
const B2_APP_KEY_ID = config.b2.appKeyId;
const B2_BUCKET_ID  = config.b2.bucketIdBook;

const REQUEST_TIMEOUT     = 1000 * 15;
const REQUEST_RETRY_DELAY = 1000 * 2;
const axiosClient         = createAxiosClient();

/**
 * @param {import("bull").Job} job
 */
module.exports = async function workerDownloadBook (job) {
  const { bookId } = job.data;

  const book = await bookRepo.findById(bookId);
  if (! book)
    throw new Error(`Book ID [${bookId}] not found.`);

  const sourceUrl = book.file.source_url;
  if (! sourceUrl) {
    // TEMPORARY;
    // throw new Error(`Book ID [${bookId}] have no file.source_url.`);
    console.error(new Error(`Book ID [${bookId}] have no file.source_url.`));
    return;
  }

  const localFilePath         = localStorage.getPathDownloadedFile(bookId);
  const { md5 }               = book.file;
  let   downloadedAndVerified = await fileExistsAndMD5Verified(localFilePath, md5);
  let   fileExt               = undefined;
  const MAX_ATTEMPTS          = 3;
  let   attempts              = 0;
  do { // Have to enter-the-loop at least 1 time to retrieve file-extension.
    const resp = await requestForDownloadStream(sourceUrl);
    fileExt = getFileExtensionFromContentDisposition(resp);
    if (downloadedAndVerified)
      break;

    unlinkSyncIfExists(localFilePath);
    console.log(`Book ID [${bookId}] Downloading book file [${localFilePath}].`);
    await job.progress(0);
    await download(resp, localFilePath, job);
    console.log(`Book ID [${bookId}] Downloaded book file.`);

    downloadedAndVerified = await fileExistsAndMD5Verified(localFilePath, md5);
    ++attempts;
  } while (! downloadedAndVerified && (attempts < MAX_ATTEMPTS));

  if (! downloadedAndVerified)
    throw new Error(`Too much attempts to download the verified book-file for #${bookId}`);

  try {
    console.log(`Book ID [${bookId}] Uploading book file`);
    const destFilename      = `${bookId}.${fileExt}`;
    const uploadRespPayload = await upload(bookId, localFilePath, destFilename);
    console.log(`Book ID [${bookId}] Uploaded book file`);

    const { fileId, downloadUrl } = uploadRespPayload;
    await bookRepo.updateB2InfoForBookFile(bookId, fileId, downloadUrl, fileExt);
    console.log(`Book ID [${bookId}] Updated B2 file-id of book file.`);

  } catch (err) {
    await job.discard();
    throw err;

  } finally {
    unlinkSyncIfExists(localFilePath);
  }
}


/**
 * @param {String} sourceUrl
 */
async function requestForDownloadStream (sourceUrl) {
  const MAX_ATTEMPTS = 3;
  let attempts       = 0;
  let resp           = null;
  const raiseAttempts = () => ++attempts;
  const shouldRetry   = () => attempts < MAX_ATTEMPTS;
  const waitFor       = async ms => await new Promise(resolve => setTimeout(resolve, ms));

  do {
    resp = await axiosClient.get(sourceUrl, { responseType: 'stream' });

    const fileExt    = getFileExtensionFromContentType(resp);
    const acceptable = (fileExt != 'html');
    if (acceptable)
      return resp;

    raiseAttempts();
    if (shouldRetry())
      await waitFor(REQUEST_RETRY_DELAY);

  } while (shouldRetry());

  const err = new Error('Could not request for download-stream.');
  err.response = resp;
  throw err;
}

/**
 * Get file-extension from response header [Content-Type];
 *
 * @param {import('axios').AxiosResponse} resp
 */
function getFileExtensionFromContentType (resp) {
  const contentType = resp.headers['content-type'];

  const fileExt = mime.extension(contentType);
  return fileExt;
}

/**
 * Get file-extension from response header [Content-Disposition];
 *
 * @param {import('axios').AxiosResponse} resp
 */
function getFileExtensionFromContentDisposition (resp) {
  const contentDisposition = resp.headers['content-disposition'];
  const fileExtRegexList = [
    /filename=".+\.(.+)";?/,
    /filename\*=.*''.+\.(.+)/,
  ];

  for (const regex of fileExtRegexList) {
    const found = regex.exec(contentDisposition);
    if (found) {
      const fileExt = found[1].toLowerCase();
      return fileExt;
    }
  }
  throw new Error('File extension not extracted');
}

async function fileExistsAndMD5Verified(path, md5) {
  return fs.existsSync(path)
    && await checksumVerifier.verifyMD5ForFile(path, md5);
}

async function download (resp, localFilePath, job) {
  const sourceStream = resp.data;
  const size         = resp.headers['content-length'];

  const updateProgInterval = setUpdateProgressInterval(5000, job, sourceStream, size);
  try {
    await localStorage.saveStreamToFile(sourceStream, localFilePath);
    job.progress(100);

  } finally {
    clearInterval(updateProgInterval);
  }
}

function unlinkSyncIfExists (path) {
  if (fs.existsSync(path))
    fs.unlinkSync(path);
}

function setUpdateProgressInterval (interval, job, downloadStream, byteSize) {
  const prog = {
    total: byteSize,
    recv : 0,
  };

  downloadStream.on('data', chunk => prog.recv += chunk.length);

  const updateProgInterval = setInterval(intervalCallback, interval);
  function intervalCallback () {
    const curPercent = parseFloat((100.0 * prog.recv/prog.total).toFixed(2));
    job.progress(curPercent);
  };

  return updateProgInterval;
}

async function upload (bookId, localFilePath, uploadFilename) {
  const b2Client = B2Client.createInstanceForBucketBooks(B2_APP_KEY, B2_APP_KEY_ID, B2_BUCKET_ID);

  const MAX_ATTEMPTS      = 3;
  let   attempts          = 0;
  let   uploadRespPayload = null;
  let   invalidB2FileId   = false;
  let   err               = null;
  const raiseAttempts = () => ++attempts;
  const shouldRetry   = () => invalidB2FileId && (attempts < MAX_ATTEMPTS);
  do {
    raiseAttempts();
    uploadRespPayload = await b2Client.upload(fs.createReadStream(localFilePath), uploadFilename, 5);

    invalidB2FileId = ! uploadRespPayload.fileId;
    if (invalidB2FileId) {
      err                   = new Error('Failed to retrieve [fileId] from response payload.');
      err.uploadRespPayload = uploadRespPayload;

      if (shouldRetry()) {
        console.error(err);
        console.log(`Retrying upload book-file to B2 for Book [${bookId}].`);
      }
    }
  } while (shouldRetry());

  if (invalidB2FileId)
    throw err;

  return uploadRespPayload;
}

function createAxiosClient () {
  const client = axios.create({
    timeout: REQUEST_TIMEOUT,
    proxy  : config.proxy.disabled ? undefined : config.proxy,
  });

  axiosRetryPlugin(client, {
    shouldResetTimeout: true,
    retryDelay    : () => REQUEST_RETRY_DELAY,
    retryCondition: () => true, // Retry all-the-types
  });

  return client;
}

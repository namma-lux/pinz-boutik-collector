'use strict';

const commander = require("commander");
const bunyan = require('bunyan');
const importer = require('../data-importers/libgen-csv/importer');
const init = require('./_init');
const localStorage = require('../file-services/local-storage');
const queueServices = require('../queues-services');

new commander.Command()
  .arguments('<filepath>')
  .action(run)
  .parse(process.argv);


async function run (filepath) {
  await init()
    .then(async () => {
      const ISO_DATE = new Date().toISOString();

      const logFileAbsPath = localStorage.getPathImportIssue(`${ISO_DATE}.import-libgen-csv.log`);
      const logger         = createLogger(logFileAbsPath);
      let   insertedCount  = 0;
      let   updatedCount   = 0;
      let   failedCount    = 0;

      function onRecordInserted (book) {
        queueServices.fetchLibgen.pushJobFetchLibgen(book.id).then(
          () => {
            ++insertedCount;
            console.log(`Book ID [${book.id}] New job [fetch-libgen] pushed to queue.`);
          },
          err => {
            logError(`Book ID [${book.id}] Failed to push new job [fetch-libgen].`);
            logError(err);
          }
        );
      }

      function onRecordUpdated (existedBook, newCategory) {
        ++updatedCount;
        console.log(`Book ID [${existedBook.id}] added new category [${newCategory}].`);
      }

      function onRecordError (err) {
        ++failedCount;
        logError(err);
      }

      function onComplete () {
        console.log(`${insertedCount} new-record(s) have been inserted.`);

        if (updatedCount)
          console.log(`${updatedCount} existed-record(s) have been updated [metadata.category].`);

        if (failedCount)
          console.log(`${failedCount} row(s) was not imported. Please view failed-records' issues in ${logFileAbsPath}.`);

        process.exit();
      }

      function onError (err) {
        logError(err);
        console.log(`Please view failed-records' issues in ${logFileAbsPath}.`);

        process.exit(1);
      }

      function logError (err) {
        logger.error(err);
        console.error(err);
      }

      importer
        .importFromLibgenCsv(filepath)
        .on('record:inserted', onRecordInserted)
        .on('record:updated', onRecordUpdated)
        .on('record:error', onRecordError)
        .on('complete', onComplete)
        .on('error', onError);
    })
    .catch(err => {
      console.error(err);
      process.exit(1);
    });
}

function createLogger (logFileAbsPath) {
  return bunyan.createLogger({
    name   : `import-libgen-csv`,
    streams: [{ level: 'error', path: logFileAbsPath }],
  });
}
'use strict';

const path = require('path');
const dotenv = require('dotenv');
const db = require('../database/mongo/db');

const config = loadEnv();

module.exports = async function init () {
  await db.connect(
    config.mongo.host,
    config.mongo.port,
    config.mongo.db,
    config.mongo.user,
    config.mongo.pwd
  );
}

module.exports.config = config;


function loadEnv () {
  const dotenvPath = path.resolve(__dirname, '../../.env');

  const dotenvLoadResult = dotenv.config({ path: dotenvPath });
  if (dotenvLoadResult.error)
    throw dotenvLoadResult.error;

  const config = require('../config');
  config.validate();

  return config;
}
'use strict';

const fs = require('fs');
const commander = require('commander');
const papaparse = require('papaparse');

const BookRating = require('../const/BookRating');
const bookRepo = require('../database/mongo/repos/book-repo');
const init = require('./_init');
const wpProductsCsvExtractor = require('../data-exporters/wp-products-csv/extractor');

new commander.Command()
  .arguments('<inputPath> <outputPath>')
  .action(run)
  .parse(process.argv);


async function run (inputPath, outputPath) {
  await init();

  const bookIds = getBookIdsFromInputPath(inputPath);

  const queryOptions = initQueryOptions(bookIds);
  const queryResult  = await bookRepo.getList(queryOptions);

  const csvRows        = extractCsvRowsData(queryResult.data);
  const csvFileContent = papaparse.unparse(csvRows, { quotes: true });

  fs.writeFileSync(outputPath, csvFileContent);

  process.exit();
}


function getBookIdsFromInputPath (inputPath) {
  const inputFileContent = fs.readFileSync(inputPath).toString();
  const bookIds          = inputFileContent.split(/\n/).map(bookId => bookId.trim()).filter(bookId => bookId.length > 0);

  return bookIds;
}

function initQueryOptions (bookIds) {
  return {
    filter: {
      _id:              bookIds,
      completionStatus: bookRepo.DATA_COMPLETION_STATUS.COMPLETED,
    },
    sorting: {
      _id: 1,
    },
  };
}

function extractCsvRowsData (books) {
  return books.map(book => {
    const ratingType                           = book.metadata.rating || BookRating.TYPE_4;
    const { defaultPriceMin, defaultPriceMax } = BookRating.getMetadata(ratingType);

    return wpProductsCsvExtractor.extractCsvRowDataFromBook(book, defaultPriceMin, defaultPriceMax);
  });
}
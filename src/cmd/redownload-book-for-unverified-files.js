'use strict';

const _init = require('./_init');
const B2Client = require('../file-services/backblaze/Client');
const bookRepo = require('../database/mongo/repos/book-repo');
const { DATA_COMPLETION_STATUS } = require('../database/mongo/repos/book-repo');
const queueServices = require('../queues-services');

const { config: { b2: b2Config } } = _init;

_init()
  .then(async () => {
    const b2 = B2Client.createInstanceForBucketBooks(b2Config.appKey, b2Config.appKeyId, b2Config.bucketIdBook);

    const filter      = { completionStatus: DATA_COMPLETION_STATUS.FILE_COLLECTED };
    const paging      = { size: 50, index: 0 };
    const sorting     = { '_id': 1 };
    let   pageRecords = [];
    do {
      const result = await bookRepo.getList({ filter, paging, sorting });
      pageRecords  = result.data;

      await Promise.all(
        pageRecords.map(async (book) => {
          const bookId   = book.id;
          const bookMd5  = book.file.md5;
          const bookSize = book.file.size;

          try {
            let   verified = false;
            const b2FileId = book.file.b2_file_id;

            const b2FileInfo = await b2.getFileInfo(b2FileId);
            const uploadedMD5  = b2FileInfo.contentMd5 && b2FileInfo.contentMd5.toUpperCase();
            const uploadedSize = b2FileInfo.contentLength;

            // Not all uploaded file has MD5,
            // but every uploaded file has content-length.
            if (uploadedMD5) {
              if (uploadedMD5 == bookMd5) {
                verified = true;
              }
            } else if (uploadedSize == bookSize) {
              verified = true;
            }

            if (! verified) {
              console.error(`${bookId} uploaded file should be redownloaded.`, JSON.stringify({ bookMd5, uploadedMD5, bookSize, uploadedSize }));
            }

            if (! verified) {
              await queueServices.downloadBook.pushJobDownloadBook(bookId)
                .then(()   => console.log(`Pushed new job [download-book] for Book [${bookId}]`))
                .catch(err => console.error(`Failed to push new job for Book [${bookId}]`))
                ;
            }
          } catch (err) {
            console.error(`Failed to retrieve b2-file-info for ${bookId}.`, err);
          }
        })
      );
      ++paging.index;
    } while (pageRecords.length > 0); // While queried result is NOT EMPTY;
  })
  .then(() => {
    console.log('DONE!');
    process.exit();
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });

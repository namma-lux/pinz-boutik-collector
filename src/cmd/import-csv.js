'use strict';

const commander = require("commander");
const bunyan = require('bunyan');
const importer = require("../data-importers/csv/importer");
const extractors = require('../data-importers/csv/extractors');
const init = require('./_init');
const localStorage = require('../file-services/local-storage');
const queueServices = require('../queues-services');

new commander.Command()
  .arguments('<dataSourceName> <filepath>')
  .action(run)
  .parse(process.argv);


async function run (dataSourceName, filepath) {
  await init()
    .then(async () => {
      const extractor = extractors.get(dataSourceName);

      const ISO_DATE = new Date().toISOString();

      const logFileAbsPath = localStorage.getPathImportIssue(`${ISO_DATE}.${dataSourceName}.log`);
      const logger         = createLogger(logFileAbsPath, dataSourceName);
      let   hasIssue       = false;
      let   insertedCount  = 0;

      function onRecordInserted (book) {
        queueServices.fetchLibgen.pushJobFetchLibgen(book.id).then(
          () => {
            ++insertedCount;
            console.log(`Book ID [${book.id}] New job [fetch-libgen] pushed to queue.`);
          },
          err => {
            logger.error(err, `Book ID [${book.id}] Failed to push new job [fetch-libgen].`);
          }
        );
      }

      function onRecordError (err) {
        logger.error(err, err.message);
      }

      function onRecordErrorOnce () {
        hasIssue = true;
      }

      function onComplete () {
        console.log(`${insertedCount} new-record(s) have been inserted.`);
        if (hasIssue)
          console.log(`Please view failed-records' issues in ${logFileAbsPath}.`);

        process.exit();
      }

      function onError (err) {
        logger.error(err);
        console.error(err);
        console.log(`Please view failed-records' issues in ${logFileAbsPath}.`);

        process.exit(1);
      }

      importer
        .importFromCsv(extractor, filepath)
        .on('record:inserted', onRecordInserted)
        .on('record:error', onRecordError)
        .once('record:error', onRecordErrorOnce)
        .on('complete', onComplete)
        .on('error', onError);
    })
    .catch(err => {
      console.error(err);
      process.exit(1);
    });
}

function createLogger (logFileAbsPath, dataSourceName) {
  return bunyan.createLogger({
    name   : `import-${dataSourceName}-csv`,
    streams: [{ level: 'error', path: logFileAbsPath }],
  });
}

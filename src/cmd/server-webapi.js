'use strict';

const _init = require('./_init');
const webserver = require('../http/webapi-server');

const { config } = _init;

_init()
  .then(() => webserver.listen(config.serverApi.port, config.serverApi.host))
  .catch(err => {
    console.error('Failed to start WebAPI server.', err);
    process.exit(1);
  });
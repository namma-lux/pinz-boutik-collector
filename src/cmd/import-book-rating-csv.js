'use strict';

const commander = require('commander');
const bunyan = require('bunyan');
const init = require('./_init');
const importer = require('../data-importers/book-rating-csv/importer');
const localStorage = require('../file-services/local-storage');

new commander.Command()
  .arguments('<filepath>')
  .action(run)
  .parse(process.argv);


async function run (filepath) {
  await init();

  const ISO_DATE       = new Date().toISOString();
  const logFileAbsPath = localStorage.getPathImportIssue(`${ISO_DATE}.import-book-rating-csv.log`);
  const logger         = createLogger(logFileAbsPath);
  let   updatedCount   = 0;
  let   failedCount    = 0;

  function onRecordUpdated (book) {
    ++updatedCount;
    console.log(`Book ID [${book.id}] updated book-rating [${book.metadata.rating}].`);
  }

  function onRecordError (err) {
    ++failedCount;
    logError(err);
  }

  function onComplete () {
    if (updatedCount)
      console.log(`${updatedCount} record(s) have been updated book-rating.`);

    if (failedCount)
      console.log(`${failedCount} row(s) was not imported. Please view failed-records' issues in ${logFileAbsPath}.`);

    process.exit();
  }

  function onError (err) {
    logError(err);
    console.log(`Please view failed-records' issues in ${logFileAbsPath}.`);

    process.exit(1);
  }

  function logError (err) {
    logger.error(err);
    console.error(err);
  }

  importer
    .importFromBookRatingCsv(filepath)
    .on('record:updated', onRecordUpdated)
    .on('record:error', onRecordError)
    .on('complete', onComplete)
    .on('error', onError);
}

function createLogger (logFileAbsPath) {
  return bunyan.createLogger({
    name:    'import-book-rating-csv',
    streams: [{ level: 'error', path: logFileAbsPath }],
  });
}
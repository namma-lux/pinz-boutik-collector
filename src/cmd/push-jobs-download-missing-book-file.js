'use strict';

const bookRepo = require('../database/mongo/repos/book-repo');
const init = require('./_init');
const queueServices = require('../queues-services');

init()
  .then(async () => {
    const queryResult = await bookRepo.getList({
      filter: {
        fetchedLibgen   : true,
        completionStatus: bookRepo.DATA_COMPLETION_STATUS.FILE_NOT_COLLECTED
      }
    });
    console.log(`-> Found ${queryResult.total} records`);

    let newJobsCount = 0;
    const promises = queryResult.data.map(
      book => queueServices.downloadBook.pushJobDownloadBook(book.id)
        .then(()   => ++newJobsCount)
        .then(()   => console.log(`Book ID [${book.id}] New job [download-book] pushed to queue.`))
        .catch(err => console.error(err, `Book ID [${book.id}] Failed to push new job [download-book].`))
    );
    await Promise.all(promises);

    console.log(`${newJobsCount} new [download-book] job(s) have been inserted into queue.`);
    process.exit();
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
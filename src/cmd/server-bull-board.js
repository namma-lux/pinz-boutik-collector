'use strict';

const { config } = require('./_init');
const bullBoardServer = require('../http/bull-board-server');

bullBoardServer.run(config.serverBull.port, config.serverBull.host)
  .catch(err => {
    console.error('Failed to start BullGUI server.', err);
    process.exit(1);
  });

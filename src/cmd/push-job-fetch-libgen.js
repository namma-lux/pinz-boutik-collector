'use strict';

const commander = require('commander');
const init = require('./_init');
const queueServices = require('../queues-services');
const bookRepo = require('../database/mongo/repos/book-repo');

new commander.Command()
  .arguments('<bookId>')
  .option('--no-download', 'Just fetching. Skip pushing jobs for downloading.')
  .action(run)
  .parse(process.argv);

async function run (bookId, cmd) {
  const noDownload = ! cmd.download;

  await init()
    .then(async () => {
      const bookNotFound = ! await bookRepo.exist(bookId);
      if (bookNotFound)
        throw new Error(`Book [${bookId}] not found.`);

      await queueServices.fetchLibgen.pushJobFetchLibgen(bookId, noDownload);
    })
    .then(() => {
      console.log(`New job has been pushed into the queue [fetch-libgen] for book [${bookId}] (--no-download ${noDownload}).`);
      process.exit();
    })
    .catch(err => {
      console.error(err);
      process.exit(1);
    });
}
'use strict';

const commander = require('commander');
const init = require('./_init');
const queueServices = require('../queues-services');

new commander.Command()
  .arguments('<queueName> [concurrency]')
  .action(run)
  .parse(process.argv);


async function run (queueName, concurrency = 1) {
  await init()
    .then(async () => {
      const { queueNames } = queueServices;
      switch (queueName) {
        case queueNames.DOWNLOAD_BOOK:
          queueServices.downloadBook.worker.start();
          break;

        case queueNames.DOWNLOAD_SMALL_FILE:
          queueServices.downloadSmallFile.worker.start();
          break;

        case queueNames.FETCH_LIBGEN:
          queueServices.fetchLibgen.worker.start();
          break;

        default:
          throw new Error(`Queue name [${queueName}] not supported.`);
      }

      console.log(`Queue worker [${queueName}] started.`);
    })
    .catch(err => {
      console.error(err);
      process.exit(1);
    });
}

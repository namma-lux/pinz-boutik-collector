'use strict';

const bullBoard = require('bull-board');
const { BullAdapter } = bullBoard;
const express = require('express');
const bullQueueFactory = require("../../queues-services/bullQueueFactory");

module.exports = { run };


async function run (port, host = undefined) {
  const addr = host ? `${host}:${port}` : port;

  bullBoard.setQueues([
    new BullAdapter(bullQueueFactory.getQueueDownloadBook()),
    new BullAdapter(bullQueueFactory.getQueueDownloadSmallFile()),
    new BullAdapter(bullQueueFactory.getQueueFetchLibgen()),
  ]);

  const app = express();
  app.use('/', bullBoard.router);
  app.listen(port, host, () => console.log(`Bull Board server started on port ${addr}.`));

  return app;
}

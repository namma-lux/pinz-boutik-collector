'use strict';

const express = require('express');

const insertionBatchesRouter = require('./controllers/insertion-batches/_router');
const booksRouter = require('./controllers/books/_router');
const websitesRouter = require('./controllers/websites/_router');
const exportCsvController = require('./controllers/export-csv/export-csv-controller');
const getBookCategoriesListController = require('./controllers/get-book-categories-list-controller');
const getBookRatingTypesListController = require('./controllers/get-book-rating-types-list-controller');
const retrieveJwtTokenController = require('./controllers/retrieve-jwt-token-controller');
const jwtAuthMiddleware = require('./middleware-auth-jwt');
const localStorage = require('../../file-services/local-storage');

const router = express.Router();

router.post('/auth/token',
  retrieveJwtTokenController.validationMiddleware,
  retrieveJwtTokenController
);

router.post('/export-csv',
  jwtAuthMiddleware,
  exportCsvController.validationMiddleware,
  exportCsvController
);
router.use('/export-csv/download',
  express.static(localStorage.ABS_PATH_EXPORTED_DIR)
);

router.use('/insertion-batches',
  jwtAuthMiddleware,
  insertionBatchesRouter
);

router.use('/books',
  jwtAuthMiddleware,
  booksRouter
);

router.use('/websites',
  jwtAuthMiddleware,
  websitesRouter
);

router.get('/book-categories',
  jwtAuthMiddleware,
  getBookCategoriesListController
);

router.get('/book-rating-types',
  jwtAuthMiddleware,
  getBookRatingTypesListController
);

module.exports = router;

'use strict';

const http = require('http');
const cors = require('cors');
const express = require('express');
const router = require('./router');
const ValidationError = require('./ValidationError');

exports.listen = async function (port, host = undefined) {
  const addr = host ? `${host}:${port}` : port;

  const app = express();
  app.use(cors());
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }));
  app.use('/', router);
  app.use(validationErrorHandler);

  await startServer(app, port, host);
  console.log(`Web API server started on ${addr}.`);

  return app;
}


async function startServer(app, port, host) {
  await new Promise((resolve, reject) => {
    const webserver = http.createServer(app)
      .listen(port, host)
      .once('error', reject)
      .once('listening', () => {
        webserver.off('error', reject);
        resolve();
      });

    webserver.setTimeout(3600 * 1000);
  });
}

/**
 * @param {ValidationError|Error} err
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').RequestHandler} next
 */
function validationErrorHandler (err, req, resp, next) {
  console.error(err);

  if (err instanceof ValidationError) {
    return resp
      .status(400)
      .json(err);
  }

  resp
    .status(500)
    .json({ message: 'Internal server error' });
}
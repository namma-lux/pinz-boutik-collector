'use strict';

const jwt = require('jsonwebtoken');

module.exports = jwtAuthMiddleware;

function jwtAuthMiddleware (req, resp, next) {
  const JWT_SECRET_KEY = '$2b$10$h3fROIXURCA4JvxmwFiBsOUFP4iT/RFJgotuczhFDExUlMvIqtVcO';
  const authHeader = req.headers.authorization;
  const token      = authHeader && authHeader.split(' ')[1];

  if (! token)
    return resp.status(401).json({ message: 'Access denied.' });

  jwt.verify(token, JWT_SECRET_KEY, (err, jwtPayload) => {
    if (err) {
      console.error(err);
      return resp.status(401).json({ message: 'Access denied.' });
    }

    req.jwtPayload = jwtPayload;
    next();
  });
}
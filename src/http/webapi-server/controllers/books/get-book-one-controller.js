'use strict';

const bookRepo = require('../../../../database/mongo/repos/book-repo');

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 * @param {import("express").NextFunction} next
 */
module.exports = async (req, resp, next) => {
  const { bookId } = req.params;

  const book = await bookRepo.findById(bookId);

  if (! book)
    return resp.status(404).json({ message: 'Book not found' });

  return resp.json({ data: book });
};
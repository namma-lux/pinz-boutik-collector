'use strict';

const FastestValidator = require('fastest-validator');
const BookType = require('../../../../const/BookType');
const bookRepo = require('../../../../database/mongo/repos/book-repo');
const userRepo = require('../../../../database/mongo/repos/user-repo');
const ValidationError = require('../../ValidationError');

const validate = prepareValidator();

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 * @param {import("express").NextFunction} next
 */
module.exports = async (req, resp, next) => {
  const paging = {
    index: req.query.page_index,
    size : req.query.page_size,
  };
  const filter  = getFilterFromRequest(req.query.filter);
  const sorting = getSortingFromRequest(req.query.sort);
  const lookups = ['history.inserted_by:name'];

  if (filter && filter.inserted_by) {
    const accEmail = filter.inserted_by;
    const employee = await userRepo.findByEmail(accEmail);

    if (! employee) {
      const queryResult = { total: 0, data: [] };
      return resp.json(queryResult);
    }

    filter.inserted_by = employee.id;
  }

  const queryResult = await bookRepo.getList({ paging, filter, sorting, lookups });
  return resp.json(queryResult);
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.validationMiddleware = async (req, resp, next) => {
  const validationResult = validate(req.query);
  if (validationResult instanceof Object)
    return next(ValidationError.fromFastestValidatorErrorMessages(validationResult));

  next();
};


function getFilterFromRequest (inputFilter) {
  if (! inputFilter)
    return null;

  const filter = {};

  if (inputFilter.search_by && inputFilter.keyword)
    filter[inputFilter.search_by] = inputFilter.keyword;

  if (inputFilter.completion_status)
    filter.completionStatus = inputFilter.completion_status;

  if (inputFilter.type)
    filter.type = inputFilter.type;

  return filter;
}

function getSortingFromRequest (inputSorting) {
  const { column, order } = inputSorting;
  return { [column]: order };
}

function prepareValidator () {
  const validator = new FastestValidator({ useNewCustomCheckerFunction: true });
  const validationSchema = {
    $$strict: 'remove',
    page_index: { optional: true, convert: true, type: 'number', integer: true, min: 0, default: 0  },
    page_size : { optional: true, convert: true, type: 'number', integer: true, min: 1, max: 100, default: 50 },
    sort: {
      optional: true,
      type    : 'object',
      strict  : 'remove',
      props: {
        column: 'string',
        order: {
          optional: 'true',
          type    : 'enum',
          values  : ['1', '-1'],
          default : 1,
          custom: value => parseInt(value),
        },
      },
    },
    filter: {
      optional: true,
      type    : 'object',
      strict  : 'remove',
      props: {
        search_by: {
          optional: true,
          type    : 'string',
          custom (value, errs, rule, path, ancestor, ctx) {
            const searchByIsEmpty = !value;
            if (ancestor.keyword && searchByIsEmpty)
              errs.push({ type: 'stringEmpty' });

            return value;
          },
        },
        keyword  : {
          optional: true,
          type    : 'string',
        },
        completion_status: {
          optional: true,
          type    : 'enum',
          values  : bookRepo.DATA_COMPLETION_STATUS.values(),
        },
        type: {
          optional: true,
          type    : 'enum',
          values  : BookType.getAvailableValues(),
        },
      },
    },
  };

  return validator.compile(validationSchema);
}
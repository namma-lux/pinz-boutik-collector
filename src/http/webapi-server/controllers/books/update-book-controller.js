'use strict';

const FastestValidator = require('fastest-validator');
const BookType = require('../../../../const/BookType');
const bookRepo = require('../../../../database/mongo/repos/book-repo');
const queueServices = require('../../../../queues-services');
const ValidationError = require('../../ValidationError');

const validate = prepareValidator();

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports = async (req, resp, next) => {
  const { bookId } = req.params;
  const newData    = await createBookDataFromRequest(req);

  const origBook = await bookRepo.findById(bookId);
  const newBook  = await bookRepo.update(bookId, newData);

  const hasNewSrcImg  = origBook.image.source_url != newBook.image.source_url;
  const hasNewSrcFile = origBook.file.source_url != newBook.file.source_url;
  if (hasNewSrcImg)
    await queueServices.downloadSmallFile.pushJobDownloadBookImage(newBook.id);
  if (hasNewSrcFile)
    await queueServices.downloadBook.pushJobDownloadBook(newBook.id);

  return resp.json({
    data: {
      ...newBook,
      _meta: {
        has_new_src_image: hasNewSrcImg,
        has_new_src_file : hasNewSrcFile,
      },
    }
  });
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.existenceCheckingMiddleware = async (req, resp, next) => {
  const { bookId } = req.params;

  const notFound = ! await bookRepo.exist(bookId);
  if (notFound)
    return resp.status(404).json({ message: 'Book not found' });

  next();
};

/**
 * TODO This function's body seems to be duplicated; IMPROVE IT!
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.validationMiddleware = async (req, resp, next) => {
  const validationResult = validate(req.body);
  if (validationResult instanceof Object)
    return next(ValidationError.fromFastestValidatorErrorMessages(validationResult));

  next();
};

/**
 * TODO This function's body seems to be duplicated; IMPROVE IT!
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.duplicationCheckingMiddleware = async (req, resp, next) => {
  const { type: bookType, isbn13, isbn10 } = req.body.identity;
  const excludedId = req.params.bookId;

  if (BookType.BOOK == bookType) {
    if (isbn10 && await bookRepo.existStandardBook({ isbn10 }, excludedId))
      next(new ValidationError({ 'identity.isbn10': [`ISBN10 [${isbn10}] is duplicated`] }));
    if (isbn13 && await bookRepo.existStandardBook({ isbn13 }, excludedId))
      next(new ValidationError({ 'identity.isbn13': [`ISBN13 [${isbn13}] is duplicated`] }));
  }

  next();
};


function prepareValidator () {
  const validator = new FastestValidator({ useNewCustomCheckerFunction: true });
  const validationSchema = {
    $$strict: 'remove',

    identity: {
      optional: true,
      type    : 'object',
      strict  : 'remove',
      props: {
        title : { optional: true, type: 'string' },
        type  : { optional: true, type: 'enum', values: BookType.getAvailableValues() },
        isbn10: { optional: true, type: 'string', length: 10 },
        isbn13: { optional: true, type: 'string', length: 13 },
      },
    },

    metadata: {
      optional: true,
      type    : 'object',
      strict  : 'remove',
      props: {
        category      : { optional: true, type: 'string' },
        author        : { optional: true, type: 'string' },
        description   : { optional: true, type: 'string' },
        edition       : { optional: true, type: 'string' },
        series        : { optional: true, type: 'string' },
        publisher     : { optional: true, type: 'string' },
        published_year: { optional: true, type: 'number', positive: true, integer: true, convert: true },
        language      : { optional: true, type: 'string' },
        pages_count   : { optional: true, type: 'number', positive: true, integer: true, convert: true },
        price         : { optional: true, type: 'number', positive: true, convert: true },
      },
    },

    image_source_url: { optional: true, type: 'string' },
    file_source_url : { optional: true, type: 'string' },
  };

  return validator.compile(validationSchema);
}

/**
 * @param {import('express').Request} req
 */
async function createBookDataFromRequest (req) {
  const {
    identity, metadata,
    image_source_url, file_source_url,
  } = req.body;

  const bookData = {};

  if (identity && (identity.isbn10 || identity.isbn13)) {
    Object.assign(bookData, identity);

    const bookType = bookData.type || await bookRepo.getBookTypeById(req.params.bookId);
    if (bookType != BookType.BOOK) {
      if (bookData.isbn10) {
        bookData.isbn10_related = bookData.isbn10;
        bookData.isbn10         = null;
      }
      if (bookData.isbn13) {
        bookData.isbn13_related = bookData.isbn13;
        bookData.isbn13         = null;
      }
    } else {
      bookData.isbn10_related = null;
      bookData.isbn13_related = null;
    }
  }

  if (metadata)
    bookData.metadata = metadata;
  if (image_source_url)
    bookData.image = { source_url: image_source_url };
  if (file_source_url)
    bookData.file = { source_url: file_source_url };

  return bookData;
}
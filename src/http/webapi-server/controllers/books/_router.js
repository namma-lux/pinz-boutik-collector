'use strict';

const express = require('express');
const getBooksListController = require('./get-books-list-controller');
const getBookOneController = require('./get-book-one-controller');
const insertBookController = require('./insert-book-controller');
const updateBookController = require('./update-book-controller');

const router = express.Router();

router.get('/',
  getBooksListController.validationMiddleware,
  getBooksListController
);

router.post('/',
  insertBookController.validationMiddleware,
  insertBookController.duplicationCheckingMiddleware,
  insertBookController
);

router.get('/:bookId', getBookOneController);

router.put('/:bookId',
  updateBookController.existenceCheckingMiddleware,
  updateBookController.validationMiddleware,
  updateBookController.duplicationCheckingMiddleware,
  updateBookController
);

module.exports = router;
'use strict';

const FastestValidator = require('fastest-validator');
const BookType = require('../../../../const/BookType');
const bookRepo = require('../../../../database/mongo/repos/book-repo');
const userRepo = require('../../../../database/mongo/repos/user-repo');
const queueServices = require('../../../../queues-services');
const ValidationError = require('../../ValidationError');

const validate = prepareValidator();

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports = async (req, resp, next) => {
  const bookData = createBookDataFromRequest(req);

  const employeeEmail = req.jwtPayload.email;
  const employee      = await userRepo.findByEmail(employeeEmail);

  const newBook = await bookRepo.insert(bookData, employee.id);

  if (newBook.image.source_url)
    await queueServices.downloadSmallFile.pushJobDownloadBookImage(newBook.id);
  if (newBook.file.source_url)
    await queueServices.downloadBook.pushJobDownloadBook(newBook.id);

  return resp.json({ data: newBook });
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.validationMiddleware = async (req, resp, next) => {
  const validationResult = validate(req.body);
  if (validationResult instanceof Object)
    return next(ValidationError.fromFastestValidatorErrorMessages(validationResult));

  next();
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.duplicationCheckingMiddleware = async (req, resp, next) => {
  const { type: bookType, isbn13, isbn10 } = req.body.identity;

  if (BookType.BOOK == bookType) {
    if (isbn10 && await bookRepo.existStandardBook({ isbn10 }))
      next(new ValidationError({ 'identity.isbn10': [`ISBN10 [${isbn10}] is duplicated`] }));
    if (isbn13 && await bookRepo.existStandardBook({ isbn13 }))
      next(new ValidationError({ 'identity.isbn13': [`ISBN13 [${isbn13}] is duplicated`] }));
  }

  next();
};


function prepareValidator () {
  const validator = new FastestValidator({ useNewCustomCheckerFunction: true });
  const validationSchema = {
    $$strict: 'remove',

    identity: {
      type  : 'object',
      strict: 'remove',
      props: {
        title : { optional: true, type: 'string' },
        type  : { type: 'enum', values: BookType.getAvailableValues() },
        isbn10: { optional: true, type: 'string', length: 10 },
        isbn13: { optional: true, type: 'string', length: 13 },
      },
    },

    metadata: {
      optional: true,
      type    : 'object',
      strict  : 'remove',
      props: {
        category      : { optional: true, type: 'string' },
        author        : { optional: true, type: 'string' },
        description   : { optional: true, type: 'string' },
        edition       : { optional: true, type: 'string' },
        series        : { optional: true, type: 'string' },
        publisher     : { optional: true, type: 'string' },
        published_year: { optional: true, type: 'number', positive: true, integer: true, convert: true },
        language      : { optional: true, type: 'string' },
        pages_count   : { optional: true, type: 'number', positive: true, integer: true, convert: true },
        price         : { optional: true, type: 'number', positive: true, convert: true },
      },
    },

    image_source_url: { optional: true, type: 'string' },
    file_source_url : { optional: true, type: 'string' },
  };

  return validator.compile(validationSchema);
}

/**
 * @param {import('express').Request} req
 */
function createBookDataFromRequest (req) {
  const {
    identity, metadata,
    image_source_url, file_source_url,
  } = req.body;

  const bookData = { ...identity };

  if (bookData.type != BookType.BOOK) {
    if (bookData.isbn10) {
      bookData.isbn10_related = bookData.isbn10;
      bookData.isbn10         = null;
    }
    if (bookData.isbn13) {
      bookData.isbn13_related = bookData.isbn13;
      bookData.isbn13         = null;
    }
  }

  if (metadata)
    bookData.metadata = metadata;
  if (image_source_url)
    bookData.image = { source_url: image_source_url };
  if (file_source_url)
    bookData.file = { source_url: file_source_url };

  return bookData;
}

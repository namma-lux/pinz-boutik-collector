'use strict';

const FastestValidator = require('fastest-validator');
const papaparse = require('papaparse');

const bookRepo = require('../../../../database/mongo/repos/book-repo');
const BookRating = require('../../../../const/BookRating');
const CategorySKU = require('../../../../const/CategorySKU');
const exportHistoryRepo = require('../../../../database/mongo/repos/export-history-repo');
const localStorage = require('../../../../file-services/local-storage');
const ValidationError = require('../../ValidationError');
const websiteRepo = require('../../../../database/mongo/repos/website-repo');
const wpProductsCsvExtractor = require('../../../../data-exporters/wp-products-csv/extractor');

const validate = prepareValidator();

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 */
module.exports = async (req, resp) => {
  const { website_id, book_categories, rating_price_map } = req.body;

  const csvRows         = [];
  const queryOptions    = initQueryOptions(book_categories);
  const incrPagingIndex = () => ++queryOptions.paging.index;
  let   pageRecords     = undefined;
  do {
    pageRecords = (await bookRepo.getList(queryOptions)).data;

    const extractedCsvRow = pageRecords.map(book => {
      const ratingType               = book.metadata.rating || BookRating.TYPE_4;
      const { price_min, price_max } = rating_price_map[ratingType];

      return wpProductsCsvExtractor.extractCsvRowDataFromBook(book, price_min, price_max);
    });
    csvRows.push(...extractedCsvRow);

    incrPagingIndex();
  } while (pageRecords.length > 0);

  const csvFileContent = papaparse.unparse(csvRows, { quotes: true });
  const csvFilename    = await saveExportedCsvFile(csvFileContent);

  resp
    .on('finish', () => insertExportHistory(website_id, book_categories))
    .json({ csvFilename });
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 */
module.exports.validationMiddleware = async function (req, resp, next) {
  const validationResult = validate(req.body);
  if (validationResult instanceof Object)
    return next(ValidationError.fromFastestValidatorErrorMessages(validationResult));

  const websiteId     = req.body.website_id;
  const websiteExists = await websiteRepo.exists(websiteId);
  if (! websiteExists)
    return next(new ValidationError({
      website_id: [`Website ID [${websiteId}] not found`]
    }))

  next();
}


function insertExportHistory (website_id, bookCategories) {
  for (const book_category of bookCategories) {
    exportHistoryRepo.insert({ website_id, book_category });
  }
}

function saveExportedCsvFile (csvFileContent) {
  const timestamp = Date.now();
  const filename  = `${timestamp}.csv`;

  localStorage.saveStringToFile(csvFileContent, localStorage.getPathExportedFile(filename));

  return filename;
}

function prepareValidator () {
  const validator = new FastestValidator({
    useNewCustomCheckerFunction: true,
    messages: {
      numberNotGreater: 'Max-price must be greater than min-price',
    },
  });

  const validationSchema = {
    $$strict: 'remove',
    website_id: {
      type:  'string',
      empty: false,
    },
    book_categories: {
      type:   'array',
      unique: true,
      min:    1,
      items: {
        type: 'string',
        enum: CategorySKU.getAvailableValues(),
      },
    },
    rating_price_map: {
      type:   'object',
      strict: 'remove',
      props: {
        // Fields added below
      }
    }
  };

  const bookRatingTypes = BookRating.getAvailableValues();
  for (const ratingType of bookRatingTypes) {
    validationSchema.rating_price_map.props[ratingType] = {
      type:   'object',
      strict: 'remove',
      props: {
        price_min: {
          type:    'number',
          min:     1,
          convert: true,
        },
        price_max: {
          optional: true,
          type:     'number',
          convert:  true,
          custom (priceMax, errs, rule, path, ancestor, ctx) {
            const priceMin = ancestor[ratingType].price_min;
            if (priceMax && ! (priceMax > priceMin))
              errs.push({ type: 'numberNotGreater' });

            return priceMax;
          },
        },
      }
    };
  }

  return validator.compile(validationSchema);
}

function initQueryOptions (categories) {
  return {
    filter: {
      categories,
      completionStatus: bookRepo.DATA_COMPLETION_STATUS.COMPLETED,
    },
    paging: {
      size: 50,
      index: 0,
    },
    sorting: {
      _id: 1,
    },
  };
}

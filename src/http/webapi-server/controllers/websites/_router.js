'use strict';

const express = require('express');
const getWebsitesListController = require('./get-websites-list-controller');
const getWebsiteExportHistoriesController = require('./get-website-export-histories-controller');

const router = express.Router();

router.get('/', getWebsitesListController);

router.get('/:websiteId/export-histories',
  getWebsiteExportHistoriesController.validationMiddleware,
  getWebsiteExportHistoriesController
);

module.exports = router;
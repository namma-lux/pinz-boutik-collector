'use strict';

const exportHistoryRepo = require('../../../../database/mongo/repos/export-history-repo');
const websiteRepo = require('../../../../database/mongo/repos/website-repo');

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = async (req, resp) => {
  const { websiteId } = req.params;

  const exportHistories = await exportHistoryRepo.findByWebsiteId(websiteId);
  return resp.json({ data: exportHistories });
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.validationMiddleware = async (req, resp, next) => {
  const { websiteId } = req.params;
  const websiteExists = await websiteRepo.exists(websiteId);
  if (! websiteExists)
    return resp.status(404).json({ message: 'Website not found' });

  next();
};
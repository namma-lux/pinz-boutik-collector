'use strict';

const websiteRepo = require('../../../../database/mongo/repos/website-repo');

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 */
module.exports = async (req, resp) => {
  const queryResult = await websiteRepo.getList();
  return resp.json(queryResult);
};
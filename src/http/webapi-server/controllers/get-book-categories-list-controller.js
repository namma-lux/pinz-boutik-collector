'use strict';

const CategorySKU = require("../../../const/CategorySKU");

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = (req, resp) => {
  const data  = CategorySKU.getAvailableValues();
  const total = data.length;

  return resp.json({ total, data });
};
'use strict';

const FastestValidator = require('fastest-validator');
const jwt = require('jsonwebtoken');
const userRepo = require("../../../database/mongo/repos/user-repo");
const ValidationError = require('../ValidationError');

const validate = prepareValidator();

/**
 * Controller
 * POST /auth/token
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = async (req, resp, next) => {
  const JWT_SECRET_KEY = '$2b$10$h3fROIXURCA4JvxmwFiBsOUFP4iT/RFJgotuczhFDExUlMvIqtVcO';
  const { email, password } = req.body;

  const user = await userRepo.findBySimpleAuth(email, password);
  if (! user)
    return resp.status(401).json({ message: 'Incorrect email or password' });

  const userProfile = {
    email: user.email,
    name : user.name,
  };
  if (user.is_master) {
    userProfile.is_master = true;
  } else {
    userProfile.roles = user.roles;
  }

  const jwt_token = jwt.sign(userProfile, JWT_SECRET_KEY, { expiresIn: '1d' });

  return resp.json({ data: { jwt_token } });
};

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 * @param {import("express").NextFunction} next
 */
module.exports.validationMiddleware = async function (req, resp, next) {
  const validationResult = validate(req.body);
  if (validationResult instanceof Object)
    return next(ValidationError.fromFastestValidatorErrorMessages(validationResult));

  return next();
};


function prepareValidator () {
  const validator = new FastestValidator({ useNewCustomCheckerFunction: true });
  const validationSchema = {
    $$strict: 'remove',
    email   : { type: 'string' },
    password: { type: 'string' },
  };

  return validator.compile(validationSchema);
}
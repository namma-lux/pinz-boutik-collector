'use strict';

const BookRating = require('../../../const/BookRating');

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = (req, resp) => {
  const data   = [];
  const values = BookRating.getAvailableValues();
  for (const ratingType of values) {
    const metadata = BookRating.getMetadata(ratingType);
    data.push({ value: ratingType, ...metadata });
  }

  const total = values.length;

  return resp.json({ total, data });
};
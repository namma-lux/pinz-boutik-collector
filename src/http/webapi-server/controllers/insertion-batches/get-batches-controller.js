'use strict';

const bookRepo = require('../../../../database/mongo/repos/book-repo');
const insertionBatchRepo = require('../../../../database/mongo/repos/insertion-batch-repo');

module.exports = async (req, resp) => {
  const queryResult = await insertionBatchRepo.getList();

  const insertionBatchIds = queryResult.data.map(batch => batch._id);

  queryResult.data = queryResult.data.map(batch => batch.toObject());

  const batches         = queryResult.data;
  const totalCounts     = await bookRepo.countForBatches(insertionBatchIds);
  const completedCounts = await bookRepo.countForBatches(insertionBatchIds, true);
  for (const batch of batches) {
    const totalCountRecord = totalCounts.find(record => record._id.equals(batch._id));
    batch.books_count_total = totalCountRecord ? totalCountRecord.count : 0;

    const completedCountRecord = completedCounts.find(record => record._id.equals(batch._id));
    batch.books_count_completed = completedCountRecord ? completedCountRecord.count : 0;
  }

  return resp.json(queryResult);
};
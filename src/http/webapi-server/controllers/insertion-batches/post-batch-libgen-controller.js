'use strict';

const { EventEmitter } = require('events');
const validate = require('validate.js');
const BookType = require('../../../../const/BookType');
const bookRepo = require('../../../../database/mongo/repos/book-repo');
const insertionBatchRepo = require('../../../../database/mongo/repos/insertion-batch-repo');
const queueServices = require('../../../../queues-services');
const ValidationError = require('../../ValidationError');

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = async (req, resp) => {
  const { acceptedLibenList, notAcceptedLibgenList } = extractIsbn13ListFromRequest(req);
  const isbn13ToErrMap = {};
  let   insertedCount  = 0;

  async function createNewInsertionBatch (req) {
    const insertionBatchName = req.body.insertion_batch_name;
    const insertionBatch     = await insertionBatchRepo.insert(insertionBatchName);

    return insertionBatch;
  }

  function deleteInsertionBatch (insertionBatch) {
    insertionBatchRepo.deleteById(insertionBatch.id);
  }

  function raiseCountAndPushQueueJob (insertedBook) {
    ++insertedCount;
    queueServices.fetchLibgen.pushJobFetchLibgen(insertedBook.id);
  };

  function pushErrorMap (err, isbn13) {
    const { name, message } = err;
    isbn13ToErrMap[isbn13] = { name, message };
  };

  const insertionBatch = await createNewInsertionBatch(req);

  const insertionProcess = insertDb(insertionBatch, acceptedLibenList)
    .on('record:inserted', raiseCountAndPushQueueJob)
    .on('record:error', pushErrorMap);
  await insertionProcess.finishPromise;

  if (! insertedCount)
    deleteInsertionBatch(insertionBatch);

  return resp.json({
    insertedCount,
    insertionsSkipped: notAcceptedLibgenList,
    insertionsFailed : isbn13ToErrMap,
  });
};

module.exports.validationMiddleware = async function (req, resp, next) {
  const constraints = {
    insertion_batch_name: {
      presence: {
        allowEmpty: false,
        message   : "^required",
      }
    },
    str_libgen_list: {
      presence: {
        allowEmpty: false,
        message   : "^required",
      }
    }
  };

  validate.async(req.body, constraints, { cleanAttributes: true })
    .then(()       => next())
    .catch(errMsgs => next(new ValidationError(errMsgs)));
};

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} resp
 * @param {import('express').NextFunction} next
 */
module.exports.duplicationCheckingMiddleware = async (req, resp, next) => {
  const insertionBatchName = req.body.insertion_batch_name;

  if (await insertionBatchRepo.exist(insertionBatchName))
    return next(new ValidationError({ 'insertion_batch_name': ['Batch name is duplicated'] }));

  next();
};


/**
 * @param {import("express").Request} req
 */
function extractIsbn13ListFromRequest (req) {
  const libgenListStr = req.body.str_libgen_list.trim();

  const acceptedLibenList    = [];
  const notAcceptedLibgenList = [];
  const extractedIsbn13List   = libgenListStr.split(/\n+/);
  for (const extractedIsbn13 of extractedIsbn13List) {
    if (extractedIsbn13.includes('libgen') === -1) {
      notAcceptedLibgenList.push(extractedIsbn13);
      continue;
    }

    acceptedLibenList.push(extractedIsbn13);
  }

  return { acceptedLibenList, notAcceptedLibgenList };
}

/**
 * @param {String} str
 */
function removeNonAlphaNumEachLine (str) {
  return str.replace(/[^\w\n]/g, '');
}

/**
 * @param {Object}        insertionBatch
 * @param {Array<String>} libgenList
 */
function insertDb (insertionBatch, libgenList) {
  const insertionProcess = new EventEmitter;

  const insertionPromises = libgenList.map(async libgen_details_url => {
    try {
      const insertedBook = await bookRepo.insert({
        insertion_batch_id: insertionBatch.id,
        libgen_details_url,
        type: BookType.BOOK
      });
      insertionProcess.emit('record:inserted', insertedBook);
    } catch (err) {
      insertionProcess.emit('record:error', err, isbn13);
    }
  });

  insertionProcess.finishPromise = Promise.all(insertionPromises);
  return insertionProcess;
}
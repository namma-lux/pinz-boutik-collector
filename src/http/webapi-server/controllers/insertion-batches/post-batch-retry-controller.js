'use strict';

const bookRepo = require('../../../../database/mongo/repos/book-repo');
const queueServices = require('../../../../queues-services');

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} resp
 */
module.exports = async (req, resp) => {
  const { batchId } = req.params;

  const filter = {
    insertion_batch_id: batchId,
    completionStatus:   bookRepo.DATA_COMPLETION_STATUS.INCOMPLETE,
  };
  const paging      = { size: 50, index: 0 };
  const sorting     = { '_id': 1 };
  let   pageRecords = [];

  let jobCountFetchLibgen   = 0;
  let jobCountDownloadBook  = 0;
  let jobCountDownloadImage = 0;

  do {
    const result = await bookRepo.getList({ filter, paging, sorting });
    pageRecords  = result.data;

    await Promise.all(
      pageRecords.map(async (book) => {
        const libgenNotFetched  = ! book.title;
        const bookNotCollected  = ! book.file.b2_file_id;
        const imageNotCollected = ! book.image.b2_file_id;

        if (libgenNotFetched) {
          await queueServices.fetchLibgen.pushJobFetchLibgen(book.id)
            .then(() => ++jobCountFetchLibgen)
            .then(() => console.log(`Retry batch [${batchId}]: Pushed job [fetch-libgen] for book [${book.id}]`))
            .catch(err => console.error(`Retry batch [${batchId}]: Failed to push job [fetch-libgen] for book [${book.id}]`));;

        } else {
          if (bookNotCollected) {
            await queueServices.downloadBook.pushJobDownloadBook(book.id)
              .then(() => ++jobCountDownloadBook)
              .then(() => console.log(`Retry batch [${batchId}]: Pushed job [download-book] for book [${book.id}]`))
              .catch(err => console.error(`Retry batch [${batchId}]: Failed to push job [download-book] for book [${book.id}]`));
          }

          if (imageNotCollected) {
            await queueServices.downloadSmallFile.pushJobDownloadBookImage(book.id)
              .then(() => ++jobCountDownloadImage)
              .then(() => console.log(`Retry batch [${batchId}]: Pushed job [download-image] for book [${book.id}]`))
              .catch(err => console.error(`Retry batch [${batchId}]: Failed to push job [download-image] for book [${book.id}]`));
          }
        }
      })
    );
    ++paging.index;
  } while (pageRecords.length > 0);

  return resp.json({
    data: {
      job_count_fetch_libgen:   jobCountFetchLibgen,
      job_count_download_book:  jobCountDownloadBook,
      job_count_download_image: jobCountDownloadImage,
    }
  });
};
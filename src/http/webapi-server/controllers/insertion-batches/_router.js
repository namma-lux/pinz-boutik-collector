'use strict';

const express = require('express');
const getBatchesController = require('./get-batches-controller');
const postBatchController = require('./post-batch-controller');
const postBatchLibgenController = require('./post-batch-libgen-controller');
const postBatchRetryController = require('./post-batch-retry-controller');

const router = express.Router();

router.get('/',
  getBatchesController
);

router.post('/libgen',
  postBatchLibgenController.validationMiddleware,
  postBatchLibgenController.duplicationCheckingMiddleware,
  postBatchLibgenController
);

router.post('/',
  postBatchController.validationMiddleware,
  postBatchController.duplicationCheckingMiddleware,
  postBatchController
);

router.post('/:batchId/retry',
  postBatchRetryController
);

module.exports = router;
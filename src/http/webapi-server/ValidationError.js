'use strict';

class ValidationError extends Error {
  constructor (errorMessages) {
    super('Invalid input');
    this.name          = this.constructor.name;
    this.errorMessages = errorMessages;
  }

  /**
   * Instantiate from an array of error-messages from `fastest-validator`.
   *
   * @param {Array<import('fastest-validator').ValidationError>} errs
   */
  static fromFastestValidatorErrorMessages (errs) {
    const outputErrMsgs = {};
    for (const err of errs) {
      const { field, message } = err;
      if (! outputErrMsgs[field])
        outputErrMsgs[field] = [];

      outputErrMsgs[field].push(message);
    }

    return new ValidationError(outputErrMsgs);
  }
}

module.exports = ValidationError;
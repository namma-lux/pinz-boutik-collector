'use strict';

const categoryToCodeMap = {
  'ebookizz':            '0000',
  'ahabuk':              1001,
  'cstb':                1002,
  'thebookriver':        1003,
  'libgen-politics':     2001,
  'libgen-health':       2002,
  'libgen-food':         2003,
  'libgen-cook':         2004,
  'libgen-cookbook':     2005,
  'libgen-financial':    2006,
  'libgen-accounting':   2007,
  'libgen-biology':      2008,
  'libgen-money':        2009,
  'libgen-business':     2010,
  'libgen-memoirs':      2011,
  'libgen-biography':    2012,
  'libgen-spirit':       2013,
  'libgen-spirituality': 2014,
  'libgen-religion':     2015,
  'dropup-1':            3000,
  'dropup-2':            3001,
  'dropup-3':            3002,
};

module.exports = {
  ...categoryToCodeMap,
  getAvailableValues,
}


function getAvailableValues () {
  return Object.keys(categoryToCodeMap);
}
'use strict';

const TYPE_BOOK            = 'book';
const TYPE_SOLUTION_MANUAL = 'solution-manual';
const TYPE_TEST_BANK       = 'test-bank';

module.exports = {
  BOOK     : TYPE_BOOK,
  SOL_MAN  : TYPE_SOLUTION_MANUAL,
  TEST_BANK: TYPE_TEST_BANK,

  getAvailableValues,
  getLabel,
  getTitleDeterminationRegexList,

  matchSolutionManual,
  matchTestBank,
};


const valueToMetadataMap = {
  [TYPE_BOOK]: {
    label: 'Book',
  },
  [TYPE_SOLUTION_MANUAL]: {
    label: 'Solution Manual',
    titleDeterminationRegexList: [
      /solution[s]?[\s\-]*manual[s]?/i,
    ],
  },
  [TYPE_TEST_BANK]: {
    label: 'Test Bank',
    titleDeterminationRegexList: [
      /test[s]?[\s\-]*bank[s]?/i,
    ],
  },
};

function getAvailableValues () {
  return Object.keys(valueToMetadataMap);
}

function getLabel (type) {
  return getMetadata(type).label;
}

function getTitleDeterminationRegexList (type) {
  return [...getMetadata(type).titleDeterminationRegexList];
}

function getMetadata (type) {
  const metadata = valueToMetadataMap[type];
  if (!metadata)
    throw new Error(`Book type [${type}] not available.`);

  return metadata;
}

function matchSolutionManual (title) {
  for (const solManRegex of getTitleDeterminationRegexList(TYPE_SOLUTION_MANUAL)) {
    if (title.match(solManRegex))
      return true;
  }

  return false;
}

function matchTestBank (title) {
  for (const testBankRegex of getTitleDeterminationRegexList(TYPE_TEST_BANK)) {
    if (title.match(testBankRegex))
      return true;
  }

  return false;
}
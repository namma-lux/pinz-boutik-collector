'use strict';

const TYPE_1 = '1';
const TYPE_2 = '2';
const TYPE_3 = '3';
const TYPE_4 = '4';

const valueToMetadataMap = {
  [TYPE_1]: {
    label: 'Type 1',
    defaultPriceMin: 19.99,
    defaultPriceMax: 21.99,
  },
  [TYPE_2]: {
    label: 'Type 2',
    defaultPriceMin: 15.99,
    defaultPriceMax: 17.99,
  },
  [TYPE_3]: {
    label: 'Type 3',
    defaultPriceMin: 12.99,
    defaultPriceMax: 14.99,
  },
  [TYPE_4]: {
    label: 'Type 4',
    defaultPriceMin: 9.99,
    defaultPriceMax: 11.99,
  },
};

const availableValues = Object.keys(valueToMetadataMap);

module.exports = {
  TYPE_1,
  TYPE_2,
  TYPE_3,
  TYPE_4,

  getAvailableValues,
  getMetadata,
  isValid,
};


function getAvailableValues () {
  return availableValues;
}

function getMetadata (type) {
  return valueToMetadataMap[type];
}

function isValid (type) {
  return (type in valueToMetadataMap);
}
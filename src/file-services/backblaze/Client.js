'use strict';

const B2 = require('backblaze-b2');
const B2UploadAny = require('@gideo-llc/backblaze-b2-upload-any');
const moment = require('moment');
B2UploadAny.install(B2);

const B2_BUCKET_NAME_BOOKS  = 'pzbooks';
const B2_BUCKET_NAME_IMAGES = 'pzimages';
const B2_BUCKET_NAME_TEST   = 'pztestupload';

class Client {
  /**
   * @param {string} appKey
   * @param {string} appKeyId
   * @param {string} bucketId
   * @param {string} bucketName
   */
  constructor (appKey, appKeyId, bucketId, bucketName) {
    this.bucketId   = bucketId;
    this.bucketName = bucketName;

    this.lastAuthTimestamp = undefined;
    this.lastAuthData      = undefined;

    this.b2 = new B2({
      applicationKey:   appKey,
      applicationKeyId: appKeyId,
    })
  }

  /**
   * @param {string} appKey
   * @param {string} appKeyId
   * @param {string} bucketId
   */
  static createInstanceForBucketBooks (appKey, appKeyId, bucketId) {
    return new Client(appKey, appKeyId, bucketId, B2_BUCKET_NAME_BOOKS);
  }

  /**
   * @param {string} appKey
   * @param {string} appKeyId
   * @param {string} bucketId
   */
  static createInstanceForBucketImages (appKey, appKeyId, bucketId) {
    return new Client(appKey, appKeyId, bucketId, B2_BUCKET_NAME_IMAGES);
  }

  /**
   * @param {string} appKey
   * @param {string} appKeyId
   * @param {string} bucketId
   */
  static createInstanceForBucketTest (appKey, appKeyId, bucketId) {
    return new Client(appKey, appKeyId, bucketId, B2_BUCKET_NAME_TEST);
  }

  async authorize (forced = false) {
    // Should request if not-yet-authorized or already-authorized-since-15mins-ago
    const shouldRequestAuth = (! this.lastAuthTimestamp) || moment().diff(this.lastAuthTimestamp, 'minutes') >= 15;

    if (forced || shouldRequestAuth) {
      const authResp = await this.b2.authorize();
      this.lastAuthData      = authResp.data;
      this.lastAuthTimestamp = Date.now();
    }

    return this.lastAuthData;
  }

  /**
   * @param {import('stream').Readable} inputStream
   * @param {string} destinationFileName
   * @param {?number} concurrency
   */
  async upload (inputStream, destinationFileName, concurrency = 1) {
    await this.authorize();
    const downloadBaseUrl = this.lastAuthData.downloadUrl;

    let uploadRespPayload = await this.b2.uploadAny({
      bucketId: this.bucketId,
      fileName: destinationFileName,
      data    : inputStream,
      concurrency,
    });
    if (uploadRespPayload.data)
      uploadRespPayload = uploadRespPayload.data;

    uploadRespPayload.downloadUrl = `${downloadBaseUrl}/file/${this.bucketName}/${destinationFileName}`;

    return uploadRespPayload;
  }

  /**
   * @param {string} fileId
   */
  async getFileInfo (fileId) {
    await this.authorize();

    const resp = await this.b2.getFileInfo({ fileId });
    return resp.data;
  }
}

module.exports = Client;

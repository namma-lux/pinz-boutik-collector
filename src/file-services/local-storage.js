'use strict';

const fs = require('fs');
const path = require('path');

const ABS_PATH_STORAGE_DIR       = path.resolve(__dirname, '../../storage');
const ABS_PATH_DOWNLOADED_DIR    = path.resolve(ABS_PATH_STORAGE_DIR, 'downloaded');
const ABS_PATH_EXPORTED_DIR      = path.resolve(ABS_PATH_STORAGE_DIR, 'exported');
const ABS_PATH_IMPORT_ISSUES_DIR = path.resolve(ABS_PATH_STORAGE_DIR, 'import-issues');
const ABS_PATH_LOGS_DIR          = path.resolve(ABS_PATH_STORAGE_DIR, 'logs');
const ABS_PATH_UPLOADED_DIR      = path.resolve(ABS_PATH_STORAGE_DIR, 'uploaded');

module.exports = {
  ABS_PATH_STORAGE_DIR,
  ABS_PATH_DOWNLOADED_DIR,
  ABS_PATH_EXPORTED_DIR,
  ABS_PATH_IMPORT_ISSUES_DIR,
  ABS_PATH_LOGS_DIR,
  ABS_PATH_UPLOADED_DIR,

  getPathDownloadedFile,
  getPathExportedFile,
  getPathImportIssue,
  getPathLogFile,

  saveStreamToFile,
  saveStringToFile,
};


function getPathDownloadedFile (filename) {
  return path.join(ABS_PATH_DOWNLOADED_DIR, filename);
}

function getPathExportedFile (filename) {
  return path.join(ABS_PATH_EXPORTED_DIR, filename);
}

function getPathImportIssue (filename) {
  return path.join(ABS_PATH_IMPORT_ISSUES_DIR, filename);
}

function getPathLogFile (filename) {
  return path.join(ABS_PATH_LOGS_DIR, filename);
}

async function saveStreamToFile (inputStream, destFilePath) {
  return await new Promise((resolve, reject) => {
    const outputStream = fs.createWriteStream(destFilePath);

    inputStream
      .pipe(outputStream)
      .on('error', err => {
        if (fs.existsSync(destFilePath))
          fs.unlinkSync(destFilePath);

        reject(err);
      })
      .on('finish', () => {
        resolve();
      });
  });
}

async function saveStringToFile (content, destFilePath) {
  await fs.promises.writeFile(destFilePath, content);
}

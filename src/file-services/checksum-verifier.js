'use strict';

const fs = require('fs').promises;
const crypto = require('crypto');

module.exports = {
  verifyMD5ForFile,
  generateMD5FromFile,
};


async function verifyMD5ForFile (path, md5) {
  const generatedMD5 = await generateMD5FromFile(path);
  const verified      = (md5 === generatedMD5);
  console.log(path, md5, generatedMD5, verified);
  return verified;
}

async function generateMD5FromFile (path) {
  const buffer = await fs.readFile(path);
  return crypto
    .createHash('md5')
    .update(buffer, 'utf8')
    .digest('hex')
    .toUpperCase();
}
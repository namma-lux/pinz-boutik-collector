'use strict';

const { EventEmitter } = require('events');
const fs = require('fs');
const papa = require('papaparse');

const bookRepo = require('../../database/mongo/repos/book-repo');
const SkippedCsvImportError = require('../errors/SkippedCsvImportError');

module.exports = { importFromBookRatingCsv };


/**
 * @param {String} filepath
 * @returns {import('events').EventEmitter}
 */
function importFromBookRatingCsv (filepath) {
  const importProcess = new EventEmitter;
  const fileStream    = fs.createReadStream(filepath);

  async function chunkHandler (csvParsingResult, parser) {
    parser.pause();
    await importCsvData(csvParsingResult.data, importProcess);
    parser.resume();
  }

  papa.parse(fileStream, {
    header:   true,
    complete: ()  => importProcess.emit('complete'),
    error:    err => importProcess.emit('error', err),
    chunk:    chunkHandler,
  });

  return importProcess;
}

/**
 * Extract data parsed from CSV file, and update book-rating of matching book records.
 *
 * @param {Array} csvRows
 * @param {import('events').EventEmitter} emitter
 */
async function importCsvData (csvRows, emitter) {
  for (const csvRow of csvRows) {
    const { _id, rating } = csvRow;

    if (! rating) {
      emitter.emit('record:error', new SkippedCsvImportError(`No rating for Book ID [${_id}]`, csvRow));
      continue;
    }

    try {
      const updatedBook = await bookRepo.updateBookRating(_id, rating);
      if (updatedBook)
        emitter.emit('record:updated', updatedBook);
      else
        emitter.emit('record:error', new Error(`Book ID [${_id}] not found.`));

    } catch (err) {
      err.csvRow  = csvRow;
      err.message = `Import for Book ID [${_id}] ${err.message}`;
      emitter.emit('record:error', err);
    }
  }
}
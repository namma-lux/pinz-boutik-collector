'use strict';

const { EventEmitter } = require('events');
const fs = require('fs');
const HtmlEntities = require('html-entities').AllHtmlEntities;
const papa = require('papaparse');
const BookType = require('../../const/BookType');
const bookRepo = require('../../database/mongo/repos/book-repo');
const SkippedCsvImportError = require('../errors/SkippedCsvImportError');

const htmlEntities = new HtmlEntities;

module.exports = { importFromLibgenCsv };


/**
 * @param {String} filepath
 * @returns {import('events').EventEmitter}
 */
function importFromLibgenCsv (filepath) {
  const importProcess = new EventEmitter;
  const fileStream    = fs.createReadStream(filepath);

  const chunkHandler = async (csvParsingResult, parser) => {
    parser.pause();
    await importCsvData(csvParsingResult.data, importProcess);
    parser.resume();
  };

  papa.parse(fileStream, {
    header: true,
    transformHeader: transformHeader,
    complete: ()  => importProcess.emit('complete'),
    error   : err => importProcess.emit('error', err),
    chunk   : chunkHandler,
  });

  return importProcess;
}

/**
 * @param {string} header Raw header content
 */
function transformHeader (header) {
  return header
    .toLowerCase()
    .trim()
    .replace(/(\s+|-)/g, '_');
}

/**
 * Extract and insert data parsed from CSV file.
 *
 * @param {Array} csvRows Parsed CSV rows.
 * @param {import('events').EventEmitter} emitter
 */
async function importCsvData (csvRows, emitter) {
  const isMongoErrDuplicated = err => err.code == 11000;

  for (const csvRow of csvRows) {
    const data             = createBookDataFromCsvRow(csvRow);
    const libgenDetailsUrl = data.libgen_details_url;

    if (! libgenDetailsUrl) {
      emitter.emit('record:error', new SkippedCsvImportError('No Libgen-details-URL found', csvRow));
      continue; // Skip;
    }

    try {
      try {
        const newBook = await bookRepo.insert(data);
        emitter.emit('record:inserted', newBook);

      } catch (err) {
        if (isMongoErrDuplicated(err)) {
          const existedRecord = await bookRepo.findByLibgenDetailsUrl(libgenDetailsUrl);
          const newCategory   = csvRow.category.trim() || null;

          const alreadyIncludedNewCategory = existedRecord.metadata.category.includes(newCategory);
          if (! alreadyIncludedNewCategory) {
            await bookRepo.addMetadataCategory(existedRecord.id, newCategory);
            emitter.emit('record:updated', existedRecord, newCategory);
          }

        } else {
          throw err;
        }
      }

    } catch (err) {
      err.csvRow = csvRow;
      emitter.emit('record:error', err);
    }
  }
}

/**
 * Extract data from a CSV row of Ebookizz
 *
 * @param {Object} csvRow
 * @param {Object} extractor
 * @returns {Object}
 */
function createBookDataFromCsvRow (csvRow) {
  const bookType = extractBookTypeFromTitle(csvRow.title);
  const category = csvRow.category.trim()          || null;
  const price    = parseFloat(csvRow.price.trim()) || null;

  return {
    title:              csvRow.title,
    type:               bookType,
    isbn10:             null,
    isbn13:             null,
    isbn10_related:     null,
    isbn13_related:     null,
    libgen_details_url: csvRow.libgen_url,

    metadata: {
      category      : category,
      author        : null,
      description   : null,
      edition       : null,
      series        : null,
      publisher     : null,
      published_year: null,
      language      : null,
      pages_count   : null,
      price         : price,
    },

    image: {},
    file:  {},
  }
}

/**
 * @param {String} csvTitle
 */
function extractBookTypeFromTitle (csvTitle) {
  const title = htmlEntities.decode(csvTitle.trim());
  let   type  = BookType.BOOK;

  if (BookType.matchSolutionManual(title)) {
    type = BookType.SOL_MAN;
  } else if (BookType.matchTestBank(title)) {
    type = BookType.TEST_BANK;
  }

  return type;
}
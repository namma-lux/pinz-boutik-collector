'use strict';

class SkippedCsvImportError extends Error {
  constructor (reason, csvRow) {
    super(`Skipped insertion: ${reason}.`);

    this.reason = reason;
    this.csvRow = csvRow;
  }
}

module.exports = SkippedCsvImportError;

'use strict';

const axios = require('axios').default;
const axiosRetryPlugin = require('axios-retry');
const BookType = require('../../../const/BookType');
const config = require('../../../config');
const extractFromLibgenDetailsPage = require('./extractFromLibgenDetailsPage');
const extractFromLibgenSearchPage  = require('./extractFromLibgenSearchPage');
const extractFromLibgenMirrorPage  = require('./extractFromLibgenMirrorPage');

const REQUEST_TIMEOUT     = 1000 * 15;
const REQUEST_RETRY_DELAY = 1000 * 2;
const axiosClient         = createAxiosClient();

module.exports = {
  extractByIsbn,
  extractByLibgenDetailsUrl,

  extractFromLibgenSearchPage,
  extractFromLibgenDetailsPage,
  extractFromLibgenMirrorPage,
};

/**
 * @param {String} isbn
 * @param {String} bookType
 */
async function extractByIsbn (isbn, bookType) {
  if ((isbn.length != 10) && (isbn.length != 13))
    throw new Error('Invalid ISBN length');

  const libgenDetailsUrl = await fetchDetailsUrlFromLibgenSearch(isbn, bookType);
  if (! libgenDetailsUrl) {
    return null;
  }

  return await extractByLibgenDetailsUrl(libgenDetailsUrl, bookType);
}

/**
 * @param {String} url
 */
async function extractByLibgenDetailsUrl (libgenDetailsUrl, bookType) {
  const libgenDetailsFetchedData = await fetchFromLibgenDetails(libgenDetailsUrl);
  const libgenMirrorFetchedData  = await fetchFromLibgenMirror(libgenDetailsFetchedData.libgen_mirror_url);

  const extractedData = {
    title: libgenDetailsFetchedData.title,
    isbn10: libgenDetailsFetchedData.isbn10,
    isbn13: libgenDetailsFetchedData.isbn13,
    metadata: {
      ...libgenDetailsFetchedData.metadata,
      ...libgenMirrorFetchedData.metadata,
    },
    image: {
      ...libgenDetailsFetchedData.image
    },
    file: {
      ...libgenDetailsFetchedData.file,
      ...libgenMirrorFetchedData.file,
    },
  };

  const filteredData = filterExtractedDataForDb(bookType, extractedData);
  return filteredData;
}

async function fetchDetailsUrlFromLibgenSearch (isbn, bookType) {
  const url  = buildUrlLibgenSearch(isbn);
  const resp = await axiosClient.get(url);
  const html = resp.data;

  const libgenDetailsUrl = extractFromLibgenSearchPage(html, bookType);
  return libgenDetailsUrl;
}

function buildUrlLibgenSearch (isbn) {
  return `http://gen.lib.rus.ec/search.php?req=${isbn}&open=0&res=25&view=detailed&phrase=1&column=def`;
}

async function fetchFromLibgenDetails (libgenDetailsUrl) {
  const resp = await axiosClient.get(libgenDetailsUrl);
  const html = resp.data;

  const extractedData = extractFromLibgenDetailsPage(html);
  return extractedData;
}

async function fetchFromLibgenMirror (libgenMirrorUrl) {
  const resp = await axiosClient.get(libgenMirrorUrl);
  const html = resp.data;

  const extractedData = extractFromLibgenMirrorPage(html);
  return extractedData;
}

function filterExtractedDataForDb (bookType, extractedData) {
  const filteredData = {};

  if (extractedData.title)
    filteredData.title = extractedData.title;

  if (bookType == BookType.BOOK) {
    if (extractedData.isbn10)
      filteredData.isbn10 = extractedData.isbn10;
    if (extractedData.isbn13)
      filteredData.isbn13 = extractedData.isbn13;
  } else {
    if (extractedData.isbn10)
      filteredData.isbn10_related = extractedData.isbn10;
    if (extractedData.isbn13)
      filteredData.isbn13_related = extractedData.isbn13;
  }

  for (const field of ['metadata', 'image', 'file']) {
    const filteredObj  = {};
    const extractedObj = extractedData[field];
    const subFields    = Object.keys(extractedObj);

    for (const subField of subFields) {
      const subFieldIsEmpty = (! extractedObj[subField]);
      if (subFieldIsEmpty)
        continue;

      filteredObj[subField] = extractedObj[subField];
    }

    if (Object.keys(filteredObj).length > 0)
      filteredData[field] = filteredObj;
  }

  return filteredData;
}

function createAxiosClient () {
  const client = axios.create({
    timeout: REQUEST_TIMEOUT,
    proxy  : config.proxy.disabled ? undefined : config.proxy,
  });

  axiosRetryPlugin(client, {
    shouldResetTimeout: true,
    retryDelay    : () => REQUEST_RETRY_DELAY,
    retryCondition: () => true, // Retry all-the-types
  });

  return client;
}

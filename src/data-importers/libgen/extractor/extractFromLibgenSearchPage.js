'use strict';

const path = require('path');
const cheerio = require('cheerio');
const BookType = require('../../../const/BookType');

module.exports = function extractFromLibgenSearchPage (html, bookType) {
  if (!bookType)
    throw new Error('Book-type is required');

  const $ = cheerio.load(html);

  const rows = extractRowsData($);
  if (!rows.length)
    return null;

  const filteredRows = filterRowsByBookType(rows, bookType);
  if (!filteredRows.length)
    return null;

  const selectedRow = selectExtractedRow(filteredRows);
  const { libgenDetailsUrl } = selectedRow;
  return libgenDetailsUrl;
};

/**
 * @param {CheerioStatic} $
 */
function extractRowsData ($) {
  const rows = [];

  $('body > table[rules="cols"]').each((i, table) => {
    const $table = $(table);
    const rowData = {
      title           : undefined,
      publishedYear   : undefined,
      pageCount       : undefined,
      fileExt         : undefined,
      libgenDetailsUrl: undefined,
    };

    $table.find('td').each((i, td) => {
      const $td = $(td);
      const innerText = $td.text().trim();
      switch (innerText) {
        case 'Title': {
          const $nextElmt = $td.next();
          const title             = $nextElmt.text().trim();
          const libgenDetailsHref = $nextElmt.find('a').attr('href').replace(/^..\//, '');
          if (title)
            rowData.title = title;
          if (libgenDetailsHref)
            rowData.libgenDetailsUrl = `http://gen.lib.rus.ec/${libgenDetailsHref}`;
          break;
        }
        case 'Year:': {
          const yearStr = $td.next().text().trim();
          const year    = isNaN(yearStr)? 0 : parseInt(yearStr);
          rowData.publishedYear = year;
          break;
        }
        case 'Pages:': {
          const pageCountsRegex = /\D*(\d*)\D*(\d*)/s;
          const nextElmtText    = $td.next().text().trim();
          const found           = pageCountsRegex.exec(nextElmtText);
          if (found) {
            const pageCount1 = found[1]? parseInt(found[1]) : 0;
            const pageCount2 = found[2]? parseInt(found[2]) : 0;
            rowData.pageCount = (pageCount1 > pageCount2)? pageCount1 : pageCount2;
          }
          break;
        }
        case 'Extension:': {
          const fileExt = $td.next().text().trim().toLowerCase();
          if (fileExt)
            rowData.fileExt = fileExt;
          break;
        }
      }
    });

    rows.push(rowData);
  });

  return rows;
}

function filterRowsByBookType (rows, bookType) {
  switch (bookType) {
    case BookType.BOOK:
      return filterRowsForNormalBookType(rows);
    case BookType.SOL_MAN:
      return filterRowsForSolutionManual(rows);
    case BookType.TEST_BANK:
      return filterRowsForTestBank(rows);
    default:
      throw new Error(`Book-type [${bookType}] not supported`);
  }
}

function filterRowsForNormalBookType (rows) {
  const exclusionRegexList = [
    ...BookType.getTitleDeterminationRegexList(BookType.SOL_MAN),
    ...BookType.getTitleDeterminationRegexList(BookType.TEST_BANK),
  ];

  let filteredRows = [...rows];
  for (const exclusionRegex of exclusionRegexList) {
    filteredRows = filteredRows.filter(row => ! row.title.match(exclusionRegex));
  }
  return filteredRows;
}

function filterRowsForSolutionManual (rows) {
  const inclusionRegexList = BookType.getTitleDeterminationRegexList(BookType.SOL_MAN);

  let filteredRows = [...rows];
  for (const inclusionRegex of inclusionRegexList) {
    filteredRows = filteredRows.filter(row => row.title.match(inclusionRegex));
  }
  return filteredRows;
}

function filterRowsForTestBank (rows) {
  const inclusionRegexList = BookType.getTitleDeterminationRegexList(BookType.TEST_BANK);

  let filteredRows = [...rows];
  for (const inclusionRegex of inclusionRegexList) {
    filteredRows = filteredRows.filter(row => row.title.match(inclusionRegex));
  }
  return filteredRows;
}

/**
 * @param {Array} rows Extracted rows data
 */
function selectExtractedRow (rows) {
  // Descending sort by published-year;
  rows.sort((row1, row2) => row2.publishedYear - row1.publishedYear);

  const latestPublishedYear = rows[0].publishedYear;
  const latestPublishedRows = rows.filter(row => row.publishedYear === latestPublishedYear);
  // Descending sort by page-count;
  latestPublishedRows.sort((row1, row2) => row2.pageCount - row1.pageCount);

  // Find the first row with [pdf] extension;
  for (const row of latestPublishedRows) {
    if (row.fileExt == 'pdf')
      return row;
  }

  // Return the first row of the sorted-list if row with [pdf] ext. not found;
  return latestPublishedRows[0];
}
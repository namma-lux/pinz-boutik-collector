'use strict';

const cheerio = require('cheerio');

module.exports = function extractFromLibgenDetailsPage (html) {
  const $ = cheerio.load(html);

  const fetchedData = {
    title: undefined,
    isbn10: undefined,
    isbn13: undefined,
    metadata: {
      author: undefined,
      edition: undefined,
      series: undefined,
      publisher: undefined,
      published_year: undefined,
      language: undefined,
      pages_count: undefined,
    },
    image: {
      ext       : undefined,
      source_url: undefined,
    },
    file: {
      size: undefined,
      ext : undefined,
      sha1: undefined,
      md5 : undefined,
    },
    libgen_mirror_url: undefined,
  };

  $('td').each((i, elmt) => {
    const $elmt     = $(elmt);
    const innerText = $elmt.text().trim();

    if (!innerText)
      return;

    switch (innerText) {
      case 'Title:':
        const title = $elmt.next().text().trim();
        if (title)
          fetchedData.title = title;
        break;

      case 'ISBN:': {
        const nextElmtText = $elmt.next().text().trim();
        if (nextElmtText) {
          const foundIsbn10 = /\b\d{10}|\d{9}X\b/.exec(nextElmtText);
          const foundIsbn13 = /\b\978-?d{10}\b/.exec(nextElmtText);

          if (foundIsbn10)
            fetchedData.isbn10 = foundIsbn10[0];
          if (foundIsbn13)
            fetchedData.isbn13 = foundIsbn13[0].replace(/-/g, '');
        }
        break;
      }

      case 'Author(s):':
        const author = $elmt.next().text().trim();
        if (author)
          fetchedData.metadata.author = author;
        break;

      case 'Edition:':
        const edition = $elmt.next().text().trim();
        if (edition)
          fetchedData.metadata.edition = edition;
        break;

      case 'Series:':
        const series = $elmt.next().text().trim();
        if (series)
          fetchedData.metadata.series = series;
        break;

      case 'Publisher:':
        const publisher = $elmt.next().text().trim();
        if (publisher)
          fetchedData.metadata.publisher = publisher;
        break;

      case 'Year:':
        const publishedYear = $elmt.next().text().trim();
        if (publishedYear)
          fetchedData.metadata.published_year = parseInt(publishedYear);
        break;

      case 'Language:':
        const language = $elmt.next().text().trim();
        if (language)
          fetchedData.metadata.language = language;
        break;

      case 'Pages (biblio\\tech):': {
        const nextElmtText = $elmt.next().text().trim();
        if (nextElmtText) {
          const splitted = nextElmtText.split('\\');
          const biblioPagesCount = splitted[0];
          const techPagesCount   = splitted[1];
          fetchedData.metadata.pages_count = parseInt(techPagesCount) || parseInt(biblioPagesCount);
        }
        break;
      }

      case 'Size:': {
        const nextElmtText = $elmt.next().text().trim();
        if (nextElmtText) {
          const found = /\((\d+) bytes\)/.exec(nextElmtText);
          if (found) {
            const fileSizeInBytes = parseInt(found[1]);
            fetchedData.file.size = fileSizeInBytes;
          }
        }
        break;
      }

      case 'Extension:': {
        const fileExt = $elmt.next().text().trim().toLowerCase();
        if (fileExt) {
          fetchedData.file.ext = fileExt;
        }
        break;
      }
    }
  });

  // Extract hashes SHA1 + MD5;
  $('table.hashes th').each((i, elmt) => {
    const $elmt = $(elmt);
    const label = $elmt.text().trim();
    switch (label) {
      case 'MD5': {
        const md5 = $elmt.next().text().trim().toUpperCase();
        if (md5)
          fetchedData.file.md5 = md5;
      }
      case 'SHA1': {
        const sha1 = $elmt.next().text().trim().toUpperCase();
        if (sha1)
          fetchedData.file.sha1 = sha1;
      }
    }
  });

  // TODO: detect the case of default image;
  const img = $('img[src^="/covers"]')[0];
  if (img) {
    const path = $(img).attr('src');
    const url  = `http://gen.lib.rus.ec/${path}`;
    const ext  = url.split('.').pop();
    fetchedData.image.ext        = ext;
    fetchedData.image.source_url = url;
  }

  const libgenMirrorLink = $('a[href^="http://library.lol"]')[0];
  if (libgenMirrorLink) {
    const url = $(libgenMirrorLink).attr('href');
    fetchedData.libgen_mirror_url = url;
  }

  return fetchedData;
};
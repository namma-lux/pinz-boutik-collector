'use strict';

const cheerio = require('cheerio');

module.exports = function extractFromLibgenMirrorPage (html) {
  const $ = cheerio.load(html);

  const fetchedData = {
    metadata: {
      description: undefined
    },
    file: {
      source_url: undefined
    },
  };
  var links = {};
  var fastStorage = [
    'Cloudflare',
    'IPFS.io',
    'Infura'
  ];

  // Extract download-link;
  $('a').each((i, elmt) => {
    links[$(elmt).text().trim()] = $(elmt).attr('href');
  });
  // fast storage get first
  for (var i=0;i<fastStorage.length;i++) {
    if (links[fastStorage[i]]) {
      fetchedData.file.source_url = links[fastStorage[i]];
      break;
    }
  }
  // if not get normal link download
  if (!fetchedData.file.source_url) {
    fetchedData.file.source_url = links['GET'];
  }

  // Extract HTML of description;
  const descriptionRegex = /^Description:\s*(.*)/;
  $('div').each((i, elmt) => {
    const innerHtml = $(elmt).html();
    const found = descriptionRegex.exec(innerHtml);
    if (found) {
      const cleanedDescriptionHtml = removeScriptTags(found[1]);
      fetchedData.metadata.description = cleanedDescriptionHtml;
      return false; // to break the loop;
    }
  });

  return fetchedData;
};

function removeScriptTags (html) {
  const $ = cheerio.load(html);
  $('script').remove();
  return $.html();
}

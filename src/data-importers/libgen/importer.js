'use strict';

const extractor = require('./extractor');
const BookType = require('../../const/BookType');
const bookRepo = require('../../database/mongo/repos/book-repo');
const LibgenNotFoundError = require('./LibgenNotFoundError');

module.exports = {
  importByIsbn,
  importByLibgenURL,

  LibgenNotFoundError,
};

/**
 * @param {Book} book
 */
async function importByIsbn (book) {
  const isbn13 = getIsbn13FromBook(book);
  const isbn10 = getIsbn10FromBook(book);
  if (! (isbn13 || isbn10))
    throw new Error(`Book ID [${book.id}] ISBN13/ISBN10 not available`);

  let extractedData = null;
  if (isbn13)
    extractedData = await extractor.extractByIsbn(isbn13, book.type);
  if (! extractedData && isbn10)
    extractedData = await extractor.extractByIsbn(isbn10, book.type);

  if (! extractedData)
    throw new LibgenNotFoundError(`Not found ISBNs [${isbn13}] & [${isbn10}] with book type [${book.type}] from Libgen`);

  const updatedBook = await bookRepo.updateByLibgenExtractedData(book._id, extractedData);
  return updatedBook;
}

/**
 * @param {String} url URL of Libgen details page.
 */
async function importByLibgenURL (book) {
  const extractedData = await extractor.extractByLibgenDetailsUrl(book.libgen_details_url, book.type);

  if (! extractedData)
    throw new LibgenNotFoundError(`Could not get details from Libgen-details-URL ${book.libgen_details_url}`);

  console.log(`Got book data from URL ${book.libgen_details_url}`);
  const updatedBook = await bookRepo.updateByLibgenExtractedData(book._id, extractedData);
  return updatedBook;
}

function getIsbn13FromBook (book) {
  if (book.type == BookType.BOOK)
    return book.isbn13;

  return book.isbn13_related;
}

function getIsbn10FromBook (book) {
  if (book.type == BookType.BOOK)
    return book.isbn10;

  return book.isbn10_related;
}
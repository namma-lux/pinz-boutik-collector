'use strict';

class LibgenNotFoundError extends Error {}

module.exports = LibgenNotFoundError;

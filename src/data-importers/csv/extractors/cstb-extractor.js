'use strict';

const { JSDOM } = require('jsdom');
const HtmlEntities = require('html-entities').AllHtmlEntities;
const BookType = require('../../../const/BookType');

const htmlEntities = new HtmlEntities;

module.exports = {
  name: 'cstb',

  extractFromDescription,
  extractFromTitle,
};


/**
 * @param {String} csvDescription
 */
function extractFromDescription (csvDescription) {
  const data = {
    isbn10: null,
    isbn13: null,
  };

  const REGEX_ISBN10_VALIDATION = /^\d{10}$|^\d{9}X$/;
  const REGEX_ISBN13_VALIDATION = /^978\-?\d{10}$/;
  const $ = new JSDOM(csvDescription).window.document;

  const isbn10LiElmt = [...$.querySelectorAll('li')].find(elmt => elmt.textContent.match(/ISBN-10/i)); // Has dash sign
  if (isbn10LiElmt) {
    const elmtExtractedContent = isbn10LiElmt.textContent.replace(/ISBN-10:\s*\?*/i, '').trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN10_VALIDATION) && !isbn.startsWith(978)) {
        data.isbn10 = isbn;
        break;
      }
    }
  }

  const isbn13LiElmt = [...$.querySelectorAll('li')].find(elmt => elmt.textContent.match(/ISBN-13/i)); // Has dash sign
  if (isbn13LiElmt) {
    const elmtExtractedContent = isbn13LiElmt.textContent.replace(/ISBN-13:\s*\?*/i, '').trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN13_VALIDATION)) {
        data.isbn13 = isbn.replace('-', '');
        break;
      }
    }
  }

  return data;
}

/**
 * @param {String} title
 */
function extractFromTitle (csvTitle) {
  const title = htmlEntities.decode(csvTitle.trim());

  const data = {
    title: title,
    type : BookType.BOOK,
  };

  if (BookType.matchSolutionManual(title)) {
    data.type = BookType.SOL_MAN;
  } else if (BookType.matchTestBank(title)) {
    data.type = BookType.TEST_BANK;
  }

  return data;
}
'use strict';

const fs = require('fs');
const path = require('path');

module.exports = { get };

function get (name) {
  const modulePath = buildExtractorPath(name);

  const moduleNotFound = fs.existsSync(modulePath);
  if (moduleNotFound)
    throw new Error(`Extractor [${name}] not available.`);

  return require(modulePath);
}

function buildExtractorPath (name) {
  return path.join(__dirname, `${name}-extractor`);
}
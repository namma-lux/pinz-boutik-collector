'use strict';

const { JSDOM } = require('jsdom');
const HtmlEntities = require('html-entities').AllHtmlEntities;
const BookType = require('../../../const/BookType');

const htmlEntities = new HtmlEntities;

const REGEX_ISBN10_SEARCH     = /\d{10}|\d{9}X/;
const REGEX_ISBN10_VALIDATION = /^\d{10}$|^\d{9}X$/;
const REGEX_ISBN13_SEARCH     = /978\-?\d{10}/;
const REGEX_ISBN13_VALIDATION = /^978\-?\d{10}$/;

module.exports = {
  name: 'ahabuk',

  extractFromDescription,
  extractFromTitle,
};


/**
 * @param {String} csvDescription
 */
function extractFromDescription (csvDescription) {
  let result = null;

  result = extractFromDescriptionByRegexForAllInOneLine(csvDescription);
  if (result.isbn10 || result.isbn13)
    return result;

  result = {
    isbn10: extractFromDescriptionIsbn10Line(csvDescription),
    isbn13: extractFromDescriptionIsbn13Line(csvDescription),
  };
  return result;
}

function extractFromDescriptionByRegexForAllInOneLine (csvDescription) {
  const result = {
    isbn10: null,
    isbn13: null,
  };

  const dom = new JSDOM(csvDescription);
  const $   = dom.window.document;
  const arrRegexSingleLine = [
    /print isbn:\s*\?*(.+) etext/i,
    /the print version of this textbook is isbn:\s*\?*(.+)\b/i,
    /^ISBN:\s*\?*(.+)/i,
  ];

  for (const regex of arrRegexSingleLine) {
    let matched = null;
    [...$.querySelectorAll('*')].find(elmt => (matched = elmt.textContent.match(regex)));

    if (! matched)
      continue;

    const arrIsbn = matched[1].split(/\s*[\/\,]\s*/);
    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN10_VALIDATION) && !isbn.startsWith(978) && !result.isbn10) {
        result.isbn10 = isbn;
      } else if (isbn.match(REGEX_ISBN13_VALIDATION) && !result.isbn13) {
        result.isbn13 = isbn;
      }
    }
  }

  return result;
}

function extractFromDescriptionIsbn10Line (csvDescription) {
  const dom = new JSDOM(csvDescription);
  const $   = dom.window.document;

  const isbn10TcellElmt = [...$.querySelectorAll('.tcell')].find(elmt => elmt.textContent.startsWith('ISBN10')); // No dash sign
  if (isbn10TcellElmt) {
    const arrIsbn = [];

    if (isbn10TcellElmt.textContent.match(REGEX_ISBN10_SEARCH)) {
      const elmtExtractedContent = isbn10TcellElmt.textContent.replace(/ISBN10:\s*\?*/i, '').trim();
      arrIsbn.push(...elmtExtractedContent.split(/\s*[,\/]\s*/));
    } else {
      const contentElmt          = isbn10TcellElmt.nextElementSibling;
      const elmtExtractedContent = contentElmt.textContent.trim();
      arrIsbn.push(...elmtExtractedContent.split(/\s*[,\/]\s*/));
    }

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN10_VALIDATION) && !isbn.startsWith(978)) {
        return isbn;
      }
    }
  }

  const isbn10LiElmt = [...$.querySelectorAll('li')].find(elmt => elmt.textContent.match(/ISBN-10/)); // Has dash sign
  if (isbn10LiElmt) {
    const elmtExtractedContent = isbn10LiElmt.textContent.replace(/ISBN-10:\s*\?*/i, '').trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN10_VALIDATION) && !isbn.startsWith(978)) {
        return isbn;
      }
    }
  }

  const isbn10ThElmt = [...$.querySelectorAll('th')].find(elmt => elmt.textContent.startsWith('ISBN-10')); // Has dash sign
  if (isbn10ThElmt) {
    const elmtExtractedContent = isbn10ThElmt.nextElementSibling.textContent.trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN10_VALIDATION) && !isbn.startsWith(978)) {
        return isbn;
      }
    }
  }

  return null;
}

function extractFromDescriptionIsbn13Line (csvDescription) {
  const dom = new JSDOM(csvDescription);
  const $   = dom.window.document;

  const isbn13TcellElmt = [...$.querySelectorAll('.tcell')].find(elmt => elmt.textContent.startsWith('ISBN13')); // No dash sign
  if (isbn13TcellElmt) {
    const arrIsbn = [];

    if (isbn13TcellElmt.textContent.match(REGEX_ISBN13_SEARCH)) {
      const elmtExtractedContent = isbn13TcellElmt.textContent.replace(/ISBN13:\s*\?*/i, '').trim();
      arrIsbn.push(...elmtExtractedContent.split(/\s*[,\/]\s*/));
    } else {
      const contentElmt          = isbn13TcellElmt.nextElementSibling;
      const elmtExtractedContent = contentElmt.textContent.trim();
      arrIsbn.push(...elmtExtractedContent.split(/\s*[,\/]\s*/));
    }

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN13_VALIDATION)) {
        return isbn.replace('-', '');
      }
    }
  }

  const isbn13LiElmt = [...$.querySelectorAll('li')].find(elmt => elmt.textContent.startsWith('ISBN-13')); // Has dash sign
  if (isbn13LiElmt) {
    const elmtExtractedContent = isbn13LiElmt.textContent.replace(/ISBN-13:\s*\?*/i, '').trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN13_VALIDATION)) {
        return isbn.replace('-', '');
      }
    }
  }

  const isbn13ThElmt = [...$.querySelectorAll('th')].find(elmt => elmt.textContent.startsWith('ISBN-13')); // Has dash sign
  if (isbn13ThElmt) {
    const elmtExtractedContent = isbn13ThElmt.nextElementSibling.textContent.trim();
    const arrIsbn              = elmtExtractedContent.split(/\s*[,\/]\s*/);

    for (const isbn of arrIsbn) {
      if (isbn.match(REGEX_ISBN13_VALIDATION)) {
        return isbn.replace('-', '');
      }
    }
  }

  return null;
}

/**
 * @param {String} title
 */
function extractFromTitle (csvTitle) {
  const title = htmlEntities.decode(csvTitle.trim());

  const data = {
    title: title,
    type : BookType.BOOK,
  };

  if (BookType.matchSolutionManual(title)) {
    data.type = BookType.SOL_MAN;
  } else if (BookType.matchTestBank(title)) {
    data.type = BookType.TEST_BANK;
  }

  return data;
}
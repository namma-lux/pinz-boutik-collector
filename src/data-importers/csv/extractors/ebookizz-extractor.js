'use strict';

const cheerio = require('cheerio');
const HtmlEntities = require('html-entities').AllHtmlEntities;
const BookType = require("../../../const/BookType");

const htmlEntities = new HtmlEntities;

module.exports = {
  name: 'ebookizz',

  extractFromDescription,
  extractFromTitle,
};


/**
 * @param {String} csvDescription
 */
function extractFromDescription (csvDescription) {
  const data = {
    isbn10: null,
    isbn13: null,
  };

  const regexIsbn10 = /^isbn-10:\s*(.+)/i;
  const regexIsbn13 = /^isbn-13:\s*(.+)/i;
  const $ = cheerio.load(csvDescription);

  let foundIsbn10 = null;
  $('*').toArray().find(elmt => (foundIsbn10 = $(elmt).text().match(regexIsbn10)));
  if (foundIsbn10)
    data.isbn10 = foundIsbn10[1];

  let foundIsbn13 = null;
  $('*').toArray().find(elmt => (foundIsbn13 = $(elmt).text().match(regexIsbn13)));
  if (foundIsbn13)
    data.isbn13 = formatExtractedIsbn13(foundIsbn13[1]);

  return data;
}

/**
 * @param {String} extractedIsbn13
 */
function formatExtractedIsbn13 (extractedIsbn13) {
  return extractedIsbn13.replace(/\-/g, '');
}


const TITLE_EXTRACTABLE_REGEX_LIST = [
  /^\(([^()]+)\) (.*)/,
  /^(test bank) for (.*)/i,
  /^(solution manual) for (.*)/i,
];

/**
 * @param {String} title
 */
function extractFromTitle (csvTitle) {
  const title = htmlEntities.decode(csvTitle.trim());

  const data = {
    title: null,
    type : null,
  };

  for (const regex of TITLE_EXTRACTABLE_REGEX_LIST) {
    const found = title.match(regex);
    if (found) {
      data.type  = formatExtractedBookType(found[1]);
      data.title = formatExtractedTitle(found[2]);
      break;
    }
  }

  if (!data.type) {
    // If [data.type] still be null, book-type does not appear on the title.
    // and the title would still be null, too;
    data.title = title;
    data.type  = BookType.BOOK;

  } else if (['original-pdf', 'ebook-pdf'].includes(data.type)) {
    data.type = BookType.BOOK;
  }

  return data;
}

/**
 * @param {String} extractedTitle
 */
function formatExtractedTitle (extractedTitle) {
  return extractedTitle
    .replace(/\s+/, ' ')
    .toLowerCase();
}

/**
 * @param {String} extractedBookType
 */
function formatExtractedBookType (extractedBookType) {
  return extractedBookType
    .replace(/\s+/, '-')
    .toLowerCase();
}

'use strict';

const fs = require('fs');
const { EventEmitter } = require('events');
const papa = require('papaparse');
const bookRepo = require('../../database/mongo/repos/book-repo');
const BookType = require('../../const/BookType');
const SkippedCsvImportError = require('../errors/SkippedCsvImportError');

module.exports = { importFromCsv };


/**
 * @param {Object} extractor
 * @param {String} filepath
 * @returns {import('events').EventEmitter}
 */
function importFromCsv (extractor, filepath) {
  const importProcess = new EventEmitter;
  const fileStream    = fs.createReadStream(filepath);

  const chunkHandler = async (csvParsingResult, parser) => {
    parser.pause();
    await importCsvData(extractor, csvParsingResult.data, importProcess);
    parser.resume();
  };

  papa.parse(fileStream, {
    header: true,
    transformHeader: transformHeader,
    complete: ()  => importProcess.emit('complete'),
    error   : err => importProcess.emit('error', err),
    chunk   : chunkHandler,
  });

  return importProcess;
}

/**
 * @param {string} header Raw header content
 */
function transformHeader (header) {
  return header
    .toLowerCase()
    .trim()
    .replace(/(\s+|-)/g, '_');
}

/**
 * Extract and insert data parsed from CSV file.
 *
 * @param {Object} extractor
 * @param {Array} csvRows Parsed CSV rows.
 * @param {import('events').EventEmitter} emitter
 */
async function importCsvData (extractor, csvRows, emitter) {
  for (const csvRow of csvRows) {
    const data = createBookDataFromCsvRow(csvRow, extractor);

    if (hasNoIsbnAvailable(data)) {
      emitter.emit('record:error', new SkippedCsvImportError('Invalid ISBN', csvRow));
      continue; // Skip;
    }

    try {
      const newBook = await bookRepo.insert(data);
      emitter.emit('record:inserted', newBook);

    } catch (err) {
      err.csvRow = csvRow;
      emitter.emit('record:error', err);
    }
  }
}

/**
 * Extract data from a CSV row of Ebookizz
 *
 * @param {Object} csvRow
 * @param {Object} extractor
 * @returns {Object}
 */
function createBookDataFromCsvRow (csvRow, extractor) {
  const { isbn10, isbn13 } = extractor.extractFromDescription(csvRow.description);
  const dataFromTitle      = extractor.extractFromTitle(csvRow.name);

  const bookType           = dataFromTitle.type;
  const selectedIsbnCouple = {};
  if (bookType == BookType.BOOK) {
    selectedIsbnCouple.isbn10         = isbn10;
    selectedIsbnCouple.isbn13         = isbn13;
  } else {
    selectedIsbnCouple.isbn10_related = isbn10;
    selectedIsbnCouple.isbn13_related = isbn13;
  }

  console.log(selectedIsbnCouple);
  if (! (isbn10 && isbn13)) console.log(csvRow.description);

  const priceStr = csvRow.sale_price || csvRow.regular_price;
  const price    = priceStr ? parseFloat(priceStr): null;

  const imgSourceUrl = csvRow.images;
  const imgExt       = imgSourceUrl.split('.').pop();

  return {
    title:          dataFromTitle.title,
    type:           bookType,
    isbn10:         null,
    isbn13:         null,
    isbn10_related: null,
    isbn13_related: null,
    ...selectedIsbnCouple,

    metadata: {
      category      : extractor.name,
      author        : null,
      description   : null,
      edition       : null,
      series        : null,
      publisher     : null,
      published_year: null,
      language      : null,
      pages_count   : null,
      price         : price,
    },

    image: {
      ext       : imgExt,
      source_url: imgSourceUrl,
    },

    file: {
      size      : null,
      ext       : null,
      md5       : null,
      source_url: null,
    }
  }
}

function hasNoIsbnAvailable (data) {
  return !data.isbn10
    && !data.isbn10_related
    && !data.isbn13
    && !data.isbn13_related;
}
'use strict';

const ordinal = require('ordinal');
const ucwords = require('ucwords');

module.exports = { transform };


/**
 * @param {string} raw
 */
function transform (raw) {
  let cleaned = raw.toLowerCase();
  cleaned = removeNonAlphaNum(cleaned);
  cleaned = removeEdAndEditionString(cleaned);
  cleaned = cleaned.trim();

  if (! cleaned)
    return null;

  cleaned = cleaned.split(/\s+/g)
    .map(parseOrdinalIfIsEditionNumber)
    .join(' ');

  return `${ucwords(cleaned)} Edition`;
}

/**
 * @param {string} input
 */
function removeNonAlphaNum (input) {
  return input.replace(/[^A-Za-z0-9\s]/g, '');
}

/**
 * @param {string} input
 */
function removeEdAndEditionString (input) {
  return input
    .replace(/edition/gi, '')
    .replace(/\bed(\b|\.)/gi, '')
    ;
}

/**
 * @param {string} token
 */
function parseOrdinalIfIsEditionNumber (token) {
  const intVal = parseInt(token);

  if (isNaN(intVal))
    return token;

  if (intVal < 1 || intVal > 99)
    return token;

  return ordinal(intVal);
}
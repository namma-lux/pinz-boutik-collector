'use strict';

const moment = require('moment');
const ucwords = require('ucwords');

const BookType = require('../../const/BookType');
const CategorySKU = require('../../const/CategorySKU');
const editionValueTransformer = require('../../data-transformers/edition-value-transformer');

module.exports = { extractCsvRowDataFromBook };

function extractCsvRowDataFromBook (book, priceMin, priceMax = undefined) {
  const postTitle =
    `${buildTitle(book)} (${getRandomDownloadLabel()})`
      .replace(/\’/g, '\'')
      .replace(/\'S/g, '\'s');

  const postContent =
    buildPostContent(book)
      .replace(/\’/g, '\'')
      .replace(/\'S/g, '\'s');

  return {
    ...getDefaultCsvRowData(),
    'post_title':         postTitle,
    'sku':                buildSku(book),
    'post_content':       postContent,
    'regular_price':      priceMax ? getRandomPriceBetween(priceMin, priceMax): priceMin,
    'images':             book.image.b2_url,
    'downloadable_files': book.file.b2_url,
    'isbn':               (book.type == BookType.BOOK) ? (book.isbn13         || '') : '',
    'isbnrelated':        (book.type != BookType.BOOK) ? (book.isbn13_related || '') : '',
  };
}


function getDefaultCsvRowData () {
  return {
    'post_title':                 '',
    'sku':                        '',
    'post_content':               '',
    'regular_price':              '',
    'sale_price':                 '',
    'images':                     '',
    'downloadable_files':         '',
    'downloadable':               'yes',
    'virtual':                    'yes',
    'post_date':                  moment().format('YYYY-MM-DD'),
    'post_status':                'publish',
    'post_name':                  '',
    'ID':                         '',
    'post_excerpt':               '',
    'menu_order':                 0,
    'post_author':                1,
    'comment_status':             'open',
    'stock':                      '',
    'weight':                     '',
    'length':                     '',
    'width':                      '',
    'height':                     '',
    'tax_class':                  '',
    'visibility':                 '',
    'stock_status':               'instock',
    'backorders':                 'no',
    'manage_stock':               'no',
    'tax_status':                 'taxable',
    'upsell_ids':                 '',
    'crosssell_ids':              '',
    'featured':                   '',
    'sale_price_dates_from':      '',
    'sale_price_dates_to':        '',
    'download_limit':             -1,
    'download_expiry':            7,
    'product_url':                '',
    'button_text':                '',
    'sold_individually':          'no',
    'low_stock_amount':           '',
    'purchase_note':              '',
    'tax:product_type':           'simple',
    'tax:product_visibility':     '',
    'tax:product_cat':            '',
    'tax:product_tag':            '',
    'tax:product_shipping_class': '',
    'isbn':                       '',
    'isbnrelated':                '',
  };
}

function buildTitle (book) {
  let title = book.title;

  const { author, edition }  = book.metadata;
  const titleContainsEdition = title.match(/edition/i);
  const transformedEdition   = edition && editionValueTransformer.transform(edition);
  if ((! titleContainsEdition) && transformedEdition)
    title += ` ${transformedEdition}`;
  if (author)
    title += ` by ${author}`;

  title = ucwords(title);

  return title;
}

function getRandomDownloadLabel () {
  const availableLabels = [
    'Download Ebook',
    'Download PDF/Epub',
    'PDF/Epub Download',
  ];
  return availableLabels[Math.floor(Math.random() * availableLabels.length)];
}

function buildSku (book) {
  const selectedCategory = book.metadata.category.split(',')[0].trim();

  const sku = CategorySKU[selectedCategory];
  if (! sku)
    throw new Error(`Category [${selectedCategory}] not supported.`);

  return `LG-${sku}-${book.id}`;
}

function buildPostContent (book) {
  const listItems = [];

  const { edition }        = book.metadata;
  const transformedEdition = edition && editionValueTransformer.transform(edition);

  if (book.metadata.series)
    listItems.push({ label: 'Series', value: ucwords( book.metadata.series) });
  if (transformedEdition)
    listItems.push({ label: 'Edition', value: transformedEdition });
  if (book.metadata.author)
    listItems.push({ label: 'Authors', value: ucwords(book.metadata.author) });
  if (book.file.size) {
    const byteSize = book.file.size;
    if (byteSize < 1000000) {
      const kbSize = (byteSize / 1000).toFixed(2);
      listItems.push({ label: 'File Size', value: `${kbSize} KB` });
    } else {
      const mbSize = (byteSize / 1000000).toFixed(2);
      listItems.push({ label: 'File Size', value: `${mbSize} MB` });
    }
  }
  if (book.file.ext)
    listItems.push({ label: 'Format', value: ucwords(book.file.ext) });
  if (book.metadata.pages_count)
    listItems.push({ label: 'Pages', value: ucwords(book.metadata.pages_count) });
  if (book.metadata.publisher)
    listItems.push({ label: 'Publisher', value: ucwords(book.metadata.publisher) });
  if (book.metadata.published_year && (book.metadata.published_year > 1000))
    listItems.push({ label: 'Published Year', value: ucwords(book.metadata.published_year) });

  if (book.type == BookType.BOOK) {
    if (book.isbn10)
      listItems.push({ label: 'ISBN-10', value: book.isbn10 });
    if (book.isbn13)
      listItems.push({ label: 'ISBN-13', value: book.isbn13 });
  } else {
    if (book.isbn10_related)
      listItems.push({ label: 'ISBN-10', value: book.isbn10_related });
    if (book.isbn13_related)
      listItems.push({ label: 'ISBN-13', value: book.isbn13_related });
  }

  const liElmts = listItems.map(listItem => `<li class="desc-meta"><strong class="desc-meta-label">${listItem.label}</strong>: <span class="desc-meta-content">${listItem.value}</span></li>`);
  const ulHtml  = `<ul class="desc-content">${liElmts.join('')}</ul>`;

  const descContentHtml = book.metadata.description
    ? `<div class="desc-content">${book.metadata.description}</div>`
    : '';

  const header     = `(${getRandomDownloadLabel()}) ${buildTitle(book)}`;
  const headerHtml = `<h2 class="desc-header">${header}</h2>`;

  const containerHtml = `<div class="desc-container">${headerHtml}${ulHtml}${descContentHtml}</div>`;

  return containerHtml;
}

function getRandomPriceBetween (priceMin, priceMax) {
  const minMaxDiff = Math.ceil(priceMax - priceMin);
  const randomDiff = Math.round(Math.random() * minMaxDiff);

  return priceMin + randomDiff;
}
